import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft, ifft
import pandas as pd
from scipy import integrate

# Import csv file
df = pd.read_csv('raw_68.csv', index_col=['Time 100us'])
print(df.head())

#plot data
accFrame = df[["AccX", "AccY", "AccZ"]]
gyroFrame = df[["GyroX", "GyroY", "GyroZ"]]


fig = plt.figure()
ax = fig.add_subplot(221)
plt.plot(accFrame)
ax = fig.add_subplot(222)
plt.plot(gyroFrame)
plt.show()
