//***************USBIO function definations *******************
#include "framework.h"
#include "USBIO.H"
#include <stdint.h>

#define MAXLEN 1024
#define MPU_IMU_REG_COUNT 7

extern UINT mIndex;

void CheckNum(UCHAR value)
{
	if(value <0 || value >255)
	{
		printf("the entry value is illegal!");
		return;
	}	
}


int16_t getSample(UCHAR readData[], int index) {
	uint16_t u = ((uint8_t)readData[2 * index] << 8) & 0xFF00;
	u = u | (((uint8_t)readData[2 * index + 1]) & 0x00FF);
	return (int16_t)u;
}

#define I2C_400khz 0X82
bool MPU6050_I2CRead(uint8_t mpu6050Address, MPU6050_Measures *sample )
{
	UCHAR writeData[MAXLEN];
	UCHAR readData[MAXLEN];
	memset(writeData, 0, MAXLEN);
	memset(readData, 0, MAXLEN);
	memset(sample, 0, sizeof(MPU6050_Measures) );

	UINT iWRLen = 0;
	UINT iRDLen = 0;
	UINT i = 0;
	UCHAR SCLK = 0;

	iWRLen = 2;
	writeData[0] = (mpu6050Address << 1) & 0xFE;
	writeData[1] = MPU6050_REG_ACCEL_XOUT_H;
	iRDLen       = 2 * 7; // 2 bytes *( 3 axles accelero + temperature + 3 axles gyro )

	if (!USBIO_StreamI2C(mIndex, iWRLen, writeData, iRDLen, readData))
	{
		printf("%d Read data fail at %d !", mpu6050Address, writeData[1]);
		return(0);
	}
	else 
	{
		sample->accX        = getSample(readData, 0);
		sample->accY        = getSample(readData, 1);
		sample->accZ        = getSample(readData, 2);
		sample->temperature = getSample(readData, 3);
		sample->gyroX       = getSample(readData, 4);
		sample->gyroY       = getSample(readData, 5);
		sample->gyroZ       = getSample(readData, 6);
		sample->time100us   = GetTickCount();

		return 1;
	}

}

bool MPU6050_I2C_SetScale(uint8_t mpu6050Address, uint8_t registerAddress, uint8_t speedIndex )
{
	UCHAR writeData[MAXLEN];
	UCHAR readData[MAXLEN];
	bool res = TRUE;

	memset(writeData, 0, MAXLEN);
	memset(readData, 0, MAXLEN);
	UINT iWRLen = 0;
	UINT iRDLen = 0;
	UCHAR SCLK = 0;


	iWRLen = 3;
	writeData[0] = mpu6050Address << 1;
	writeData[1] = registerAddress;
	writeData[2] = (speedIndex << 3) & 0x18;
	iRDLen = 0;
	if (!USBIO_StreamI2C(mIndex, iWRLen, writeData, iRDLen, readData))
	{
		//MessageBox(GetActiveWindow(), _T("Set scale failed"), _T("Error"), MB_OK);
		res = FALSE;
	}
	else {
		MessageBox(GetActiveWindow(), _T("Set scale Success"), _T("Information"), MB_OK);
	}

	return res;
}

bool MPU6050_I2C_Init(uint8_t mpu6050Address)
{
	UCHAR writeData[MAXLEN];
	UCHAR readData[MAXLEN];
	bool res = TRUE;

	memset(writeData, 0, MAXLEN);
	memset(readData, 0, MAXLEN);
	UINT iWRLen = 0;
	UINT iRDLen = 0;
	UCHAR SCLK = 0;

	iWRLen = 2;
	writeData[0] = (mpu6050Address << 1) & 0xFE;
	writeData[1] = MPU6050_REG_WHOAMI;
	iRDLen = 2;
	printf("Trying to identify MPU6050  %X:%i (/2=%X:%i)  %X:%i\n", writeData[0], writeData[0], writeData[0] / 2, writeData[0] / 2, writeData[1], writeData[1]);
	if (!USBIO_StreamI2C(mIndex, iWRLen, writeData, iRDLen, readData))
	{
		printf("%i Read identification failed!\n", mpu6050Address);
		MessageBox(GetActiveWindow(), _T("Can't read identification id"), _T("Error"), MB_OK);
		return FALSE;
	}
	else {
		if (readData[0] == 0x68) {
			MessageBox(GetActiveWindow(), _T("Found MPU6050"), _T("Information"), MB_OK);
		}
		else {
			printf("Found unknown component  %X  %X\n", readData[0], readData[1]);
			MessageBox(GetActiveWindow(), _T("Unknown identification id"), _T("Error"), MB_OK);
			return FALSE;
		}
	}

	iWRLen = 3;
	writeData[0] = mpu6050Address << 1;
	writeData[1] = MPU6050_REG_PWR_MGMT_1;
	writeData[2] = 0;
	iRDLen = 0;
	if (!USBIO_StreamI2C(mIndex, iWRLen, writeData, iRDLen, readData))
	{
		printf("%i set power mode failed!\n", mpu6050Address);
		MessageBox(GetActiveWindow(), _T("Set power mode failed"), _T("Error"), MB_OK);
		res = FALSE;
	}
	else {
		printf("%i set power mode Success !\n", mpu6050Address);
		//MessageBox(GetActiveWindow(), _T("Set power mode Success"), _T("Information"), MB_OK);
	}


	/*
		// Set Clock Source
		setClockSource(MPU6050_CLOCK_PLL_XGYRO);
		// Set Scale & Range
		setScale(scale);
		setRange(range);

		*/
	printf("Initialization at address %i returns %i\n", mpu6050Address, res);
	return res;
}


