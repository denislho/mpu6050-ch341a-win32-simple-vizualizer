//{{NO_DEPENDENCIES}}
// fichier Include Microsoft Visual C++.
// Utilis� par MPU6050.rc
//
#define IDC_MYICON                      2
#define IDD_MPU6050_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MPU6050                     107
#define IDI_SMALL                       108
#define IDC_MPU6050                     109
#define IDR_MAINFRAME                   128

#define ID_REFRESH                      32770
#define ID_FICHIER_MPU60500             32771
#define ID_FICHIER_MPU60500_0           32773
#define ID_FICHIER_MPU60500_1           32774
#define ID_FICHIER_CONNECT              32776

#define ID_MPU6050_ACC_2G               32800
#define ID_MPU6050_ACC_4G               32801
#define ID_MPU6050_ACC_8G               32802
#define ID_MPU6050_ACC_16G              32803

#define ID_MPU6050_GYRO_250             32810
#define ID_MPU6050_GYRO_500             32811
#define ID_MPU6050_GYRO_1000            32812
#define ID_MPU6050_GYRO_2000            32813

#define ID_DRAW_ACC_1          32820
#define ID_DRAW_ACC_2          32821
#define ID_DRAW_ACC_12         32822
#define ID_DRAW_GYRO_1         32823
#define ID_DRAW_GYRO_2         32824
#define ID_DRAW_GYRO_12        32825
#define ID_DRAW_ACC_GYRO_1     32826
#define ID_DRAW_ACC_GYRO_2     32827
#define ID_DRAW_ALL            32830
#define ID_DRAW_MEASURE        32840
#define ID_DRAW_INTEGRAL       32841
#define ID_DRAW_INTEGRAL2      32842
#define ID_DRAW_DUPLICATE_12   32872
#define ID_DRAW_DUPLICATE_21   32874

#define ID_DRAW_FFT_NOWINDOW   32880
#define ID_DRAW_FFT_TRIANGLE   32881
#define ID_DRAW_FFT_HANN       32882
#define ID_DRAW_FFT_HAMMING    32883
#define ID_DRAW_FFT_BLACKMAN   32884

#define ID_FICHIER_REALTIME_START_1   32850
#define ID_FICHIER_REALTIME_START_5   32855
#define ID_FICHIER_REALTIME_START_10  32856
#define ID_FICHIER_REALTIME_START_20  32857
#define ID_FICHIER_REALTIME_START_50  32858
#define ID_FICHIER_REALTIME_START_100 32859

#define ID_FICHIER_REALTIME_STOP   32860
#define ID_FICHIER_REALTIME_SAMPLE 32861
#define ID_FICHIER_REALTIME_START_Thread 32862

#define ID_FICHIER_READ_1      32901
#define ID_FICHIER_READ_2      32902
#define ID_FICHIER_READ_4      32904
#define ID_FICHIER_READ_8      32908
#define ID_FICHIER_READ_16     32916
#define ID_FICHIER_READ_32     32932

#define ID_FICHIER_AUTO_INIT_68   32934
#define ID_FICHIER_AUTO_INIT_69   32935
#define ID_FICHIER_AUTO_INIT_BOTH 32936

#define ID_FICHIER_READ_AVG_1  32951
#define ID_FICHIER_READ_AVG_2  32952
#define ID_FICHIER_READ_AVG_4  32954
#define ID_FICHIER_READ_AVG_8  32958
#define ID_FICHIER_READ_AVG_16 32966
#define ID_FICHIER_READ_AVG_32 32982

#define ID_SAVE_SAVE           33000
#define ID_SAVE_RELOAD         33001
#define ID_SAVE_TEST_INTEGRAL  33002
#define ID_SAVE_PYTHON3        33003

#define ID_FILTER_RAW          33011
#define ID_FILTER_AVG_3        33013
#define ID_FILTER_AVG_5        33015
#define ID_FILTER_CENTER_0     33020
#define ID_FILTER_NO_CENTER_0  33021

#define ID_CHANNEL_A1X     33100
#define ID_CHANNEL_A1Y     33101
#define ID_CHANNEL_A1Z     33102
#define ID_CHANNEL_G1X     33103
#define ID_CHANNEL_G1Y     33104
#define ID_CHANNEL_G1Z     33105
#define ID_CHANNEL_A2X     33106
#define ID_CHANNEL_A2Y     33107
#define ID_CHANNEL_A2Z     33108
#define ID_CHANNEL_G2X     33109
#define ID_CHANNEL_G2Y     33110
#define ID_CHANNEL_G2Z     33111

#define ID_CHANNEL_DETAILS_1 33120
#define ID_CHANNEL_DETAILS_2 33121

#define ID_ZOOM_YZOOM      33200
#define ID_ZOOM_SPLIT      33210
#define ID_ZOOM_SPLIT_LARGE 33212
#define ID_ZOOM_UNSPLIT    33219
#define ID_ZOOM_Y2_BY_1    33231
#define ID_ZOOM_Y2_BY_2    33232
#define ID_ZOOM_Y2_BY_5    33235
#define ID_ZOOM_Y2_BY_10   33240
#define ID_ZOOM_Y2_BY_20   33250

#define ID_ZOOM_X_EQ_0     33300
#define ID_ZOOM_X_BY_1     33301
#define ID_ZOOM_X_BY_2     33302
#define ID_ZOOM_X_BY_4     33304
#define ID_ZOOM_X_BY_8     33308
#define ID_ZOOM_X_BY_16    33316
#define ID_ZOOM_X_BY_32    33332
#define ID_ZOOM_X_BY_64    33364

#define ID_LOAD_GPS        33400
#define ID_LOAD_CANBUS     33410



#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
