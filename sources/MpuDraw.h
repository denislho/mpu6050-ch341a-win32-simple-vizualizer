#pragma once

#include "MPU6050.h"
#include "framework.h"

#define MAX_LOADSTRING 100

#define DETAILS_TEXT_WIDTH 100

#define ZOOM_UNITY 1000

#define MAX_SAMPLES 2048

#define REALTIME_SAMPLE_COUNT 1

int zoomAll();
bool testIntegral();
bool reload(const char* fileName, MPU6050_Measures* samples, MPU6050_Measures* speed, MPU6050_Measures* position);
bool saveSamplesBloc(const char* fileName, MPU6050_Measures* samples, MPU6050_Measures* speed, MPU6050_Measures* position);
bool computeIntegral(MPU6050_Measures* samples, MPU6050_Measures* integral, int rescale);
bool computeAverage(MPU6050_Measures* samples, MPU6050_Measures* average, int count);
bool computeAllIntegrals();
bool saveSamples(char* filename);
/**
* Read values from MPU6050  and stores them in MPU6050_Samples[]
* In case of subsampling, this routine may take more than 30 seconds
* Fills the complete storage available.
*/
bool loadSamplingData(int subSample, int sampleCount, int startIndex, bool average);
void drawStringAndInt(HDC hdc, const char* str, int i, int x, int y);
void drawOneInt(HDC hdc, int i, int x, int y);

#define LINE_HEIGHT 15

void drawOneSampleDetails(HWND hWnd, HDC hdc, int x0, int y0, MPU6050_Measures* sample);
void drawDetails(HWND hWnd, HDC hdc, int x0, int y0);
void drawScales(HWND hWnd, HDC hdc, int x0, int y0);
// Difficult to use at it require to repaint the complete window
void drawMousePosition(HWND hWnd, HDC hdc, int x0, int y0);
void drawAvgData(HWND hWnd, HDC hdc, int x0, int y0, int i, int v1, int v2, int v3, int v4, int v5, int lineAverage);
void drawSamplingAccData(HWND hWnd, HDC hdc, int x0, int y0, MPU6050_Measures* samples, bool showX, bool showY, bool showZ);
void drawSamplingGyroData(HWND hWnd, HDC hdc, int x0, int y0, MPU6050_Measures* samples, bool showX, bool showY, bool showZ);