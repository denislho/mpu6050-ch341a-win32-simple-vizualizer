
#include "MpuDraw.h"
#include <windowsx.h>
#include <stdint.h>
#include <cstdlib>
#include "framework.h"
#include "MPU6050.h"
#include "resource.h"
#include "USBIO.H"
#include <realtimeapiset.h>

extern HWND hScales;
// select what to draw
extern bool showMPU6050_1_Acc;
extern bool showMPU6050_2_Acc;
extern bool showMPU6050_1_Gyro;
extern bool showMPU6050_2_Gyro ;

extern bool realtimeMode;
extern int  realtimeIndex;

extern int realtimeAveraging;

extern int threadCurrentSampleIndex;

extern int filterByAverage;

// Record point when mouse left button is pressed and released
extern int MouseDownX, MouseDownY, MouseUpX, MouseUpY;
// Record point when mouse left button is pressed while  <SHIFT> is pressed
extern int MouseDownShiftYLeft, MouseDownShiftYRight;

extern int xAxle1YPos, xAxle2YPos;

// settings sent to MPU6050
extern int currentAccelerationRatio;
extern int currentGyroscopeRatio;

extern int zoom;
extern int zoom2; // multiplier to be applied on zoom

extern int xZoom;
extern int xCenter;
extern int index100usAtCenter;
extern uint32_t xLegendScale;
extern uint32_t xLegendScale2;


extern uint8_t mpu6050Address;
extern uint8_t mpu6050Address2;

extern bool centerDrawingToZero;

// Identify the USB->I2C device used
extern UINT mIndex;
/**
* Sampling data storage
*/
MPU6050_Measures MPU6050_Samples[MAX_SAMPLES];
MPU6050_Measures MPU6050_Samples2[MAX_SAMPLES];
MPU6050_Measures MPU6050_Speed[MAX_SAMPLES];
MPU6050_Measures MPU6050_Speed2[MAX_SAMPLES];
MPU6050_Measures MPU6050_Position[MAX_SAMPLES];
MPU6050_Measures MPU6050_Position2[MAX_SAMPLES];

MPU6050_Measures MPU6050_FFT[MAX_SAMPLES];
MPU6050_Measures MPU6050_FFT2[MAX_SAMPLES];

MPU6050_Measures MPU6050_LowLevel[MAX_SAMPLES];
MPU6050_Measures MPU6050_LowLevel2[MAX_SAMPLES];

int Displayed_Time[MAX_SAMPLES];

MPU6050_Measures* MPU6050_DisplayData = MPU6050_Samples;
MPU6050_Measures* MPU6050_DisplayData2 = MPU6050_Samples2;
MPU6050_Measures MPU6050_DisplayAverage;
MPU6050_Measures MPU6050_DisplayAverage2;

extern HPEN hPenAX, hPenAY, hPenAZ, hPenGX, hPenGY, hPenGZ;

extern HWND  cbA1X, cbA1Y, cbA1Z, cbG1X, cbG1Y, cbG1Z;
extern HWND  cbA2X, cbA2Y, cbA2Z, cbG2X, cbG2Y, cbG2Z;

/**
* Compute integration of samples into integral, applying a division factor of rescale.
* Use very simple area way to calculate values. 
* Note : time is in 1/10s of millisecond
* Please note also that time is just copied to allow display and further integration is needed.
*/
bool computeIntegral(MPU6050_Measures* samples, MPU6050_Measures* integral, int rescale)
{
    bool res = FALSE;

    if (!samples || !integral) {
        return FALSE;
    }

    int64_t iax = 0;
    int64_t iay = 0;
    int64_t iaz = 0;
    int64_t igx = 0;
    int64_t igy = 0;
    int64_t igz = 0;

    uint32_t dt = 0;
    uint32_t previous = samples->time100us;

    // as sampling is not perfectly timed, calculate de kind of correction
    uint32_t timeScale =  (samples[MAX_SAMPLES - 1].time100us - samples[0].time100us) / MAX_SAMPLES;
    if (timeScale <= 1) {
        timeScale = 1;
    }
    MPU6050_Measures average;
    if (centerDrawingToZero) {
        computeAverage(samples, &average, MAX_SAMPLES);
    }
    else {
        average.accX = 0;
        average.accY = 0;
        average.accZ = 0;
        average.gyroX = 0;
        average.gyroY = 0;
        average.gyroZ = 0;
    }

    for (int i = 1; i < MAX_SAMPLES; i++) {
        dt = samples->time100us - previous;
        previous = samples->time100us;
        iax += (int64_t)dt * (int64_t)(samples->accX - average.accX);
        iay += (int64_t)dt * (int64_t)(samples->accY - average.accY);
        iaz += (int64_t)dt * (int64_t)(samples->accZ - average.accZ);
        igx += (int64_t)dt * (int64_t)(samples->gyroX - average.gyroX);
        igy += (int64_t)dt * (int64_t)(samples->gyroY - average.gyroY);
        igz += (int64_t)dt * (int64_t)(samples->gyroZ - average.gyroZ);

        integral->accX = (int32_t)((iax) / rescale / timeScale);
        integral->accY = (int32_t)((iay) / rescale / timeScale);
        integral->accZ = (int32_t)((iaz) / rescale / timeScale);
        integral->gyroX = (int32_t)((igx) / rescale / timeScale);
        integral->gyroY = (int32_t)((igy) / rescale / timeScale);
        integral->gyroZ = (int32_t)((igz) / rescale / timeScale);
        integral->time100us = samples->time100us;

        samples++;
        integral++;
    }
    return true;
}

/**
* Compute the average of each parameter of samples, and store the result in average.
* Acceleration X, Y, Z and Gyroscope X, Y, Z
*/
bool computeAverage(MPU6050_Measures* samples, MPU6050_Measures* average, int count)
{
    if (!samples) {
        return FALSE;
    }
    if (count < 0 || count> MAX_SAMPLES) {
        count = MAX_SAMPLES;
    }
    // Calculate average
    int64_t avg_ax = 0;
    int64_t avg_ay = 0;
    int64_t avg_az = 0;
    int64_t avg_gx = 0;
    int64_t avg_gy = 0;
    int64_t avg_gz = 0;
    MPU6050_Measures* ptr = samples;
    for (int i = 0; i < count; i++) {
        avg_ax += (int64_t)ptr->accX;
        avg_ay += (int64_t)ptr->accY;
        avg_az += (int64_t)ptr->accZ;
        avg_gx += (int64_t)ptr->gyroX;
        avg_gy += (int64_t)ptr->gyroY;
        avg_gz += (int64_t)ptr->gyroZ;
        ptr++;
    }
    average->accX  = avg_ax / count;
    average->accY  = avg_ay / count;
    average->accZ  = avg_az / count;
    average->gyroX = avg_gx / count;
    average->gyroY = avg_gy / count;
    average->gyroZ = avg_gz / count;
    return TRUE;
}

int zoomAll()
{
    int minY = 2147483647;
    int maxY = -2147483647;

    MPU6050_Measures* ptr1 = MPU6050_DisplayData;
    MPU6050_Measures* ptr2 = MPU6050_DisplayData2;
    for (int i = 0; i < MAX_SAMPLES; i++) {
        if (showMPU6050_1_Acc) {
            if (ptr1->accX > maxY) { maxY = ptr1->accX; }  if (ptr1->accX < minY) { minY = ptr1->accX; }
            if (ptr1->accY > maxY) { maxY = ptr1->accY; }  if (ptr1->accY < minY) { minY = ptr1->accY; }
            if (ptr1->accZ > maxY) { maxY = ptr1->accZ; }  if (ptr1->accZ < minY) { minY = ptr1->accZ; }
        }
        if (showMPU6050_1_Gyro) {
            if (ptr1->gyroX > maxY) { maxY = ptr1->gyroX; }  if (ptr1->gyroX < minY) { minY = ptr1->gyroX; }
            if (ptr1->gyroY > maxY) { maxY = ptr1->gyroY; }  if (ptr1->gyroY < minY) { minY = ptr1->gyroY; }
            if (ptr1->gyroZ > maxY) { maxY = ptr1->gyroZ; }  if (ptr1->gyroZ < minY) { minY = ptr1->gyroZ; }
        }
        if (showMPU6050_2_Acc) {
            if (ptr2->accX > maxY) { maxY = ptr2->accX; }  if (ptr2->accX < minY) { minY = ptr2->accX; }
            if (ptr2->accY > maxY) { maxY = ptr2->accY; }  if (ptr2->accY < minY) { minY = ptr2->accY; }
            if (ptr2->accZ > maxY) { maxY = ptr2->accZ; }  if (ptr2->accZ < minY) { minY = ptr2->accZ; }
        }
        if (showMPU6050_2_Gyro) {
            if (ptr2->gyroX > maxY) { maxY = ptr2->gyroX; }  if (ptr2->gyroX < minY) { minY = ptr2->gyroX; }
            if (ptr2->gyroY > maxY) { maxY = ptr2->gyroY; }  if (ptr2->gyroY < minY) { minY = ptr2->gyroY; }
            if (ptr2->gyroZ > maxY) { maxY = ptr2->gyroZ; }  if (ptr2->gyroZ < minY) { minY = ptr2->gyroZ; }
        }
        ptr1++;
        ptr2++;
    }

    if (maxY == minY) {
        return 1;
    }
    else {
        int zoom = 300 * ZOOM_UNITY / (maxY - minY);
        if (zoom < 1) {
            zoom = 1;
        }
        return zoom;
    }
}

/**
* Calculate integrations
* - raw data -> speed
* - speed -> position
* For both MPU6050
*/
bool computeAllIntegrals()
{
    bool b1 = computeIntegral(MPU6050_Samples, MPU6050_Speed, 1);
    bool b2 = computeIntegral(MPU6050_Samples2, MPU6050_Speed2, 1);
    bool b3 = computeIntegral(MPU6050_Speed, MPU6050_Position, 1);
    bool b4 = computeIntegral(MPU6050_Speed2, MPU6050_Position2, 1);
    return b1 && b2 && b3 && b4;
}


/*
* Display a line containing a text str and the integer i at position (x,y)
*/
void drawStringAndInt(HDC hdc, const char* str, int i, int x, int y)
{
    RECT rc = { x, y, x + 200, y + 25 };
    char label[128];
    sprintf_s(label, sizeof(label), "%s %i", str, i);

    const size_t cSize = strlen(label) + 1;
    wchar_t* wc = new wchar_t[cSize];
    size_t rv;
    mbstowcs_s(&rv, wc, cSize, label, _TRUNCATE);
    DrawText(hdc, wc, -1, &rc, DT_SINGLELINE);
    delete wc;
}
void drawStringAndIntTwice(HDC hdc, const char* str, int i, const char* str2, int i2, int x, int y)
{
    RECT rc = { x, y, x + 200, y + 25 };
    char label[128];
    sprintf_s(label, sizeof(label), "%s %i%s%i", str, i, str2, i2);

    const size_t cSize = strlen(label) + 1;
    wchar_t* wc = new wchar_t[cSize];
    size_t rv;
    mbstowcs_s(&rv, wc, cSize, label, _TRUNCATE);
    DrawText(hdc, wc, -1, &rc, DT_SINGLELINE);
    delete wc;
}
inline int getAcceleroMilliG(int rawValue)
{
    return rawValue * 1000 * currentAccelerationRatio / 16384;
}
inline int getGyroMilliDegSec(int rawValue)
{
    return rawValue * 1000 * currentGyroscopeRatio / 16384;
}

void drawStringAndAccelero(HDC hdc, const char* str, int i, int x, int y)
{
    drawStringAndInt(hdc, str, getAcceleroMilliG(i), x, y);
}
void drawStringAndGyro(HDC hdc, const char* str, int i, int x, int y)
{
    drawStringAndInt(hdc, str, getGyroMilliDegSec(i), x, y);
}
// Used to disply both possible values
void drawAccAndGyro(HDC hdc, int i, int i2, int x, int y)
{
    drawStringAndIntTwice(hdc, "Acc/Gyro", getAcceleroMilliG(i), "/", getGyroMilliDegSec(i2), x, y);
}
/*
* Display integer i at position(x,y)
*/
void drawOneInt(HDC hdc, int i, int x, int y)
{
    RECT rc = { x, y, x + 100, y + 25 };
    TCHAR label[32];
    memset(label, 0, sizeof(label));
    _itot_s(i, label, 16, 10);
    DrawText(hdc, label, -1, &rc, DT_SINGLELINE);
}

#define LINE_HEIGHT 15
/*
* Display details for one sample (AX, AY, AZ, GX, GY, GZ, Time) if
* the display is required.
*/
void drawOneSampleDetails(HWND hWnd, HDC hdc, int x0, int y0, MPU6050_Measures* sample)
{
    if (showMPU6050_1_Acc || showMPU6050_2_Acc) {
        SetTextColor(hdc, RGB(255, 0, 0)); drawStringAndInt(hdc, "AX", sample->accX, x0, y0); y0 += LINE_HEIGHT;
        SetTextColor(hdc, RGB(0, 255, 0)); drawStringAndInt(hdc, "AY", sample->accY, x0, y0); y0 += LINE_HEIGHT;
        SetTextColor(hdc, RGB(0, 0, 255)); drawStringAndInt(hdc, "AZ", sample->accZ, x0, y0); y0 += LINE_HEIGHT;
    }
    if (showMPU6050_1_Gyro || showMPU6050_2_Gyro) {
        SetTextColor(hdc, RGB(255, 64, 64)); drawStringAndInt(hdc, "GX", sample->gyroX, x0, y0); y0 += LINE_HEIGHT;
        SetTextColor(hdc, RGB(64, 255, 64)); drawStringAndInt(hdc, "GY", sample->gyroY, x0, y0); y0 += LINE_HEIGHT;
        SetTextColor(hdc, RGB(64, 64, 255)); drawStringAndInt(hdc, "GZ", sample->gyroZ, x0, y0); y0 += LINE_HEIGHT;
    }
    int timeValue;
    const char* text;
    if (xLegendScale != 0) {
        timeValue = xLegendScale * sample->time100us;
        text = "[x]";
    }
    else {
        timeValue = sample->time100us - MPU6050_DisplayData[0].time100us;
        text = "DT";
    }
    SetTextColor(hdc, RGB(0, 0, 0)); drawStringAndInt(hdc, text, timeValue, x0, y0); y0 += LINE_HEIGHT;
}

/*
* Draw details and also line of checkboxes based on configuration
*/
void drawDetails(HWND hWnd, HDC hdc, int x0, int y0)
{
    int yRef = y0;
    int localMouseDownX;
    int localMouseUpX;
    if (xZoom == 1) {
        localMouseDownX = MouseDownX;
        localMouseUpX = MouseUpX;
    }
    else {
        int dx = (MouseDownX - xCenter);
        int dt = dx / xZoom;
        localMouseDownX = index100usAtCenter + dt;
        dx = (MouseUpX - xCenter);
        dt = dx / xZoom;
        localMouseUpX = index100usAtCenter + dt;
    }
    bool b = IsDlgButtonChecked(hWnd, ID_CHANNEL_DETAILS_1) == BST_CHECKED;
    if (mpu6050Address && b) {
        y0 += LINE_HEIGHT; // Skip checkbox
        drawOneSampleDetails(hWnd, hdc, x0                     , y0, &(MPU6050_DisplayData[localMouseDownX]));
        drawOneSampleDetails(hWnd, hdc, x0 + DETAILS_TEXT_WIDTH, y0, &(MPU6050_DisplayData[localMouseUpX]));
        ShowWindow(cbA1X, showMPU6050_1_Acc ? SW_SHOW : SW_HIDE);
        ShowWindow(cbA1Y, showMPU6050_1_Acc ? SW_SHOW : SW_HIDE);
        ShowWindow(cbA1Z, showMPU6050_1_Acc ? SW_SHOW : SW_HIDE);
        ShowWindow(cbG1X, showMPU6050_1_Gyro ? SW_SHOW : SW_HIDE);
        ShowWindow(cbG1Y, showMPU6050_1_Gyro ? SW_SHOW : SW_HIDE);
        ShowWindow(cbG1Z, showMPU6050_1_Gyro ? SW_SHOW : SW_HIDE);
    }
    else {
        ShowWindow(cbA1X, SW_HIDE);
        ShowWindow(cbA1Y, SW_HIDE);
        ShowWindow(cbA1Z, SW_HIDE);
        ShowWindow(cbG1X, SW_HIDE);
        ShowWindow(cbG1Y, SW_HIDE);
        ShowWindow(cbG1Z, SW_HIDE);
    }

    int xCol = x0 + 2 * DETAILS_TEXT_WIDTH;
    int textLine = 0;
    drawStringAndInt(hdc, "zoom Y", zoom, xCol, y0 + textLine++ * LINE_HEIGHT);
    drawStringAndInt(hdc, "zoom Y2", zoom * zoom2, xCol, y0 + textLine++ *LINE_HEIGHT);
    drawStringAndInt(hdc, "zoom X", xZoom, xCol, y0 + textLine++ * LINE_HEIGHT);

    drawStringAndInt(hdc, "xCenter ", xCenter, xCol, y0 + textLine++ * LINE_HEIGHT);
    drawStringAndInt(hdc, "TCenter", index100usAtCenter, xCol, y0 + textLine++ * LINE_HEIGHT);
    drawStringAndInt(hdc, "X1", MouseDownX/xZoom, xCol, y0 + textLine++ * LINE_HEIGHT);
    drawStringAndInt(hdc, "X2", MouseUpX / xZoom, xCol, y0 + textLine++ * LINE_HEIGHT);
    drawStringAndInt(hdc, "Y1", (xAxle1YPos -MouseDownShiftYLeft)* ZOOM_UNITY/zoom, xCol, y0 + textLine++ * LINE_HEIGHT);
    drawStringAndInt(hdc, "Y2", (xAxle2YPos -MouseDownShiftYRight) * ZOOM_UNITY / zoom, xCol, y0 + textLine++ * LINE_HEIGHT);
    drawStringAndInt(hdc, "dY", (MouseDownShiftYLeft - MouseDownShiftYRight), xCol, y0 + textLine++ * LINE_HEIGHT);

    textLine = 3;
    if (xLegendScale == 0) { // time data
        int dx = MPU6050_DisplayData[MouseUpX].time100us - MPU6050_DisplayData[MouseDownX].time100us;
        int dt = dx / 10 / xZoom;
        drawStringAndInt(hdc, "DT ms", dt, x0 + 3 * DETAILS_TEXT_WIDTH, y0 + textLine++ * LINE_HEIGHT);
        if (dx != 0) {
            int mHz = 1000000 / dt;
            drawStringAndInt(hdc, "mHz", mHz, x0 + 3 * DETAILS_TEXT_WIDTH, y0 + textLine++ * LINE_HEIGHT);
        }
    }
    else { // FFT
        // take time from sampling, not from FFT
        int dx = MPU6050_Samples[MAX_SAMPLES].time100us - MPU6050_Samples[0].time100us;
        drawStringAndInt(hdc, "Time", dx, x0 + 3 * DETAILS_TEXT_WIDTH, y0 + textLine++ * LINE_HEIGHT);
        drawStringAndInt(hdc, "xL1 scale", xLegendScale, x0 + 3 * DETAILS_TEXT_WIDTH, y0 + textLine++ * LINE_HEIGHT);
        drawStringAndInt(hdc, "xL2 scale", xLegendScale2, x0 + 3 * DETAILS_TEXT_WIDTH, y0 + textLine++ * LINE_HEIGHT);
        if(MouseDownX > 0 ){
            int hz = (MouseDownX-1) / xZoom * xLegendScale; //FIXME : remove this -1 due to FFT output
            drawStringAndInt(hdc, "L1 : mHz", hz, x0 + 3 * DETAILS_TEXT_WIDTH, y0 + textLine++ * LINE_HEIGHT);
        }
        if (MouseUpX > 0) {
            int hz = (MouseUpX-1) / xZoom * xLegendScale; //FIXME : remove this -1 due to FFT output
            drawStringAndInt(hdc, "L2 : mHz", hz, x0 + 3 * DETAILS_TEXT_WIDTH, y0 + textLine++ * LINE_HEIGHT);
        }

    }




    b = IsDlgButtonChecked(hWnd, ID_CHANNEL_DETAILS_2) == BST_CHECKED;
    if (mpu6050Address2 && b) {
        x0 += 600;
        y0 = yRef;
        y0 += LINE_HEIGHT; //Skip checkbox
        drawOneSampleDetails(hWnd, hdc, x0                     , y0, &(MPU6050_DisplayData2[localMouseDownX]));
        drawOneSampleDetails(hWnd, hdc, x0 + DETAILS_TEXT_WIDTH, y0, &(MPU6050_DisplayData2[localMouseUpX]));
        ShowWindow(cbA2X, showMPU6050_2_Acc ? SW_SHOW : SW_HIDE);
        ShowWindow(cbA2Y, showMPU6050_2_Acc ? SW_SHOW : SW_HIDE);
        ShowWindow(cbA2Z, showMPU6050_2_Acc ? SW_SHOW : SW_HIDE);
        ShowWindow(cbG2X, showMPU6050_2_Gyro ? SW_SHOW : SW_HIDE);
        ShowWindow(cbG2Y, showMPU6050_2_Gyro ? SW_SHOW : SW_HIDE);
        ShowWindow(cbG2Z, showMPU6050_2_Gyro ? SW_SHOW : SW_HIDE);
    }
    else {
        ShowWindow(cbA2X, SW_HIDE);
        ShowWindow(cbA2Y, SW_HIDE);
        ShowWindow(cbA2Z, SW_HIDE);
        ShowWindow(cbG2X, SW_HIDE);
        ShowWindow(cbG2Y, SW_HIDE);
        ShowWindow(cbG2Z, SW_HIDE);
    }
    y0 = yRef;
}

inline int convertTime2X(int time100us)
{
        return time100us * xZoom;
}
/*
* Calculate data Y using screen y and screen X axle yCenter.
*/
int convertScreenY2Data(int yCenter, int y)
{
    return (yCenter - y) * 1000 / zoom; // to avoid rounding problems, dispaly uses a kind of fixed point *1000
}
void drawScales(HWND hWnd, HDC hdc, int x0, int y0) {

    // Horizontal axle is 3 points wide
    MoveToEx(hdc, x0, y0 - 1, NULL);    LineTo(hdc, x0 + MAX_SAMPLES, y0 - 1);
    MoveToEx(hdc, x0, y0, NULL);    LineTo(hdc, x0 + MAX_SAMPLES, y0);
    MoveToEx(hdc, x0, y0 + 1, NULL);    LineTo(hdc, x0 + MAX_SAMPLES, y0 + 1);

    // Horizontal lines used for graphics
    if (MouseDownShiftYLeft > 0) { MoveToEx(hdc, x0, MouseDownShiftYLeft, NULL);    LineTo(hdc, x0 + MAX_SAMPLES, MouseDownShiftYLeft); }
    if (MouseDownShiftYRight > 0) { MoveToEx(hdc, x0, MouseDownShiftYRight, NULL);    LineTo(hdc, x0 + MAX_SAMPLES, MouseDownShiftYRight); }
    // Show real Y taking zoom value into calculation !
    // yscreen = y0 - yData * zoom / ZOOM_UNITY / 100
    // yData   = (yscreen -y0) / zoom * ZOOM_UNITY*100
    int y = convertScreenY2Data(y0, MouseDownShiftYLeft);
    if (MouseDownShiftYLeft > 0) { drawAccAndGyro(hdc, y, y, MAX_SAMPLES / 2, MouseDownShiftYLeft - 5); }
    y = convertScreenY2Data(y0, MouseDownShiftYRight);
    if (MouseDownShiftYRight > 0) { drawAccAndGyro(hdc, y, y, MAX_SAMPLES / 2, MouseDownShiftYRight - 5); }
 
    uint32_t start = MPU6050_DisplayData[0].time100us;
    if (start == 0) {
        return;
    }
    // Draw periodic time indicators
    uint32_t restart = start;
    uint32_t elapsed = MPU6050_DisplayData[MAX_SAMPLES - 1].time100us - MPU6050_DisplayData[0].time100us;
    uint32_t tickInterval = elapsed / 20 / xZoom;
    tickInterval = tickInterval / 100;
    tickInterval = tickInterval * 100;
    uint32_t previousDt = 0;
    int tickCount = 0;
    for (int i = 0; i < MAX_SAMPLES; i += 10) {
        uint32_t dt = MPU6050_DisplayData[i].time100us - restart;
        if (dt > tickInterval) {
            int tl;
            if (tickCount % 2) {
                tl = 10;
                int x = Displayed_Time[i];
                RECT rc = { x + 2,y0 + 5, x + 100,y0 + 25 };
                TCHAR label[32];
                if (xLegendScale == 0) {
                    _itot_s((MPU6050_DisplayData[i].time100us - start) / 10, label, 10);
                    DrawText(hdc, label, -1, &rc, DT_SINGLELINE);
                }
                else {
                    _itot_s( i * xLegendScale / 1000, label, 10); // Hz
                    DrawText(hdc, label, -1, &rc, DT_SINGLELINE);
                }
                
            }
            else {
                tl = 5;
            }
            MoveToEx(hdc, x0 + i, y0 - tl, NULL); LineTo(hdc, x0 + i, y0 + tl);
            restart = MPU6050_DisplayData[i].time100us;
            tickCount++;
        }
    }
}
// Difficult to use at it require to repaint the complete window
void drawMousePosition(HWND hWnd, HDC hdc, int x0, int y0) {
    MoveToEx(hdc, x0 + MouseDownX, y0 - 300, NULL);
    LineTo(hdc, x0 + MouseDownX, y0 + 400);
    MoveToEx(hdc, x0 + MouseUpX, y0 - 300, NULL);
    LineTo(hdc, x0 + MouseUpX, y0 + 400);
}

void drawAvgData(HWND hWnd, HDC hdc, int x0, int y0, int i, int v1, int v2, int v3, int v4, int v5, int lineAverage, bool drawLine, int yZoom)
{
    int avg;
    if (filterByAverage == 3) {
        avg = (v3 + v4 + v5) / 3;
    }
    else     if (filterByAverage == 5) {
        avg = (v1 + v2 + v3 + v4 + v5) / 5;
    }
    else {
        avg = v5;
    }
    if (centerDrawingToZero) {
        avg = avg - lineAverage;
    }
    // do not draw lines out of the  screen, just move the pointer.
    int x = x0 + i * xZoom;
    Displayed_Time[i] = x;
    if (drawLine ){ //&& x0 >= 0) {
        LineTo(hdc, x, y0 - avg * yZoom / ZOOM_UNITY);
    }
    else {
        MoveToEx(hdc, x, y0 - avg * yZoom / ZOOM_UNITY, NULL);
    }
}

// getIndex = gI
int inline gI(int index) {
    if (index >= 0) {
        return index;
    }
    else {
        return 0;
    }
}
void drawSamplingAccData(HWND hWnd, HDC hdc, int x0, int y0, MPU6050_Measures* samples, bool showX, bool showY, bool showZ ) {
    HGDIOBJ hOldPen = SelectObject(hdc, hPenAX);
    int start = realtimeIndex - REALTIME_SAMPLE_COUNT;
    if (start < 0) {
        start = 0;
    }

    if (xZoom > 1) {
        // we want x0 + xCenter*xZoom = xCenter to keep center under the mouse
        // so x0 = xCenter * (1-xZoom)
        x0 = x0 - xCenter * (xZoom-1) +10;
    }
    else {
        x0 = x0;
    }
    
    if (showX) {
        start--;
        drawAvgData(hWnd, hdc, x0, y0, start, samples[gI(start - 4)].accX, samples[gI(start - 3)].accX, samples[gI(start - 2)].accX, samples[gI(start - 1)].accX, samples[gI(start)].accX, MPU6050_DisplayAverage.accX, FALSE, zoom);
        start++;
        for (int i = start; i < MAX_SAMPLES; i++) {
            if (samples[i].time100us == 0) { break;  }
            drawAvgData(hWnd, hdc, x0, y0, i, samples[gI(i - 4)].accX, samples[gI(i - 3)].accX, samples[gI(i - 2)].accX, samples[gI(i - 1)].accX, samples[gI(i)].accX, MPU6050_DisplayAverage.accX, TRUE, zoom);
        }
    }
    if (showY) {
        SelectObject(hdc, hPenAY);
        start--;
        drawAvgData(hWnd, hdc, x0, y0, start, samples[gI(start - 4)].accY, samples[gI(start - 3)].accY, samples[gI(start - 2)].accY, samples[gI(start - 1)].accY, samples[gI(start)].accY, MPU6050_DisplayAverage.accY, FALSE, zoom);
        start++;
        for (int i = start; i < MAX_SAMPLES; i++) {
            if (samples[i].time100us == 0) { break; }
            drawAvgData(hWnd, hdc, x0, y0, i, samples[gI(i - 4)].accY, samples[gI(i - 3)].accY, samples[gI(i - 2)].accY, samples[gI(i - 1)].accY, samples[gI(i)].accY, MPU6050_DisplayAverage.accY, TRUE, zoom);
        }
    }
    if (showZ) {
        SelectObject(hdc, hPenAZ);
        start--;
        drawAvgData(hWnd, hdc, x0, y0, start, samples[gI(start - 4)].accZ, samples[gI(start - 3)].accZ, samples[gI(start - 2)].accZ, samples[gI(start - 1)].accZ, samples[gI(start)].accZ, MPU6050_DisplayAverage.accZ, FALSE, zoom);
        start++;
        for (int i = start; i < MAX_SAMPLES; i++) {
            if (samples[i].time100us == 0) { break; }
            drawAvgData(hWnd, hdc, x0, y0, i, samples[gI(i - 4)].accZ, samples[gI(i - 3)].accZ, samples[gI(i - 2)].accZ, samples[gI(i - 1)].accZ, samples[gI(i)].accZ, MPU6050_DisplayAverage.accZ, TRUE, zoom);
        }
    }
    SelectObject(hdc, hOldPen);
}
void drawSamplingGyroData(HWND hWnd, HDC hdc, int x0, int y0, MPU6050_Measures* samples, bool showX, bool showY, bool showZ) 
{
    HGDIOBJ hOldPen = SelectObject(hdc, hPenGX);
    int start = realtimeIndex- REALTIME_SAMPLE_COUNT;
    if (start < 0) {
        start = 0;
    }
    if (xZoom > 1) {
        x0 = x0 - xCenter * (xZoom - 1) +10;
    }
    else {
        x0 = x0;
    }
    if (showX) {
        start--;
        drawAvgData(hWnd, hdc, x0, y0, start, samples[gI(start - 4)].gyroX, samples[gI(start - 3)].gyroX, samples[gI(start - 2)].gyroX, samples[gI(start - 1)].gyroX, samples[gI(start)].gyroX, MPU6050_DisplayAverage.gyroX, FALSE, zoom*zoom2);
        start++;
        for (int i = start; i < MAX_SAMPLES; i++) {
            if (samples[i].time100us == 0) { break; }
            drawAvgData(hWnd, hdc, x0, y0, i, samples[gI(i - 4)].gyroX, samples[gI(i - 3)].gyroX, samples[gI(i - 2)].gyroX, samples[gI(i - 1)].gyroX, samples[gI(i)].gyroX, MPU6050_DisplayAverage.gyroX, TRUE, zoom * zoom2);
        }
    }
    if (showY) {
        SelectObject(hdc, hPenGY);
        start--;
        drawAvgData(hWnd, hdc, x0, y0, start, samples[gI(start - 4)].gyroY, samples[gI(start - 3)].gyroY, samples[gI(start - 2)].gyroY, samples[gI(start - 1)].gyroY, samples[gI(start)].gyroY, MPU6050_DisplayAverage.gyroY, FALSE, zoom * zoom2);
        start++;
        for (int i = start; i < MAX_SAMPLES; i++) {
            if (samples[i].time100us == 0) { break; }
            drawAvgData(hWnd, hdc, x0, y0, i, samples[gI(i - 4)].gyroY, samples[gI(i - 3)].gyroY, samples[gI(i - 2)].gyroY, samples[gI(i - 1)].gyroY, samples[gI(i)].gyroY, MPU6050_DisplayAverage.gyroY, TRUE, zoom * zoom2);
        }
    }
    if (showZ) {
        SelectObject(hdc, hPenGZ);
        start--;
        drawAvgData(hWnd, hdc, x0, y0, start, samples[gI(start - 4)].gyroZ, samples[gI(start - 3)].gyroZ, samples[gI(start - 2)].gyroZ, samples[gI(start - 1)].gyroZ, samples[gI(start)].gyroZ, MPU6050_DisplayAverage.gyroZ, FALSE, zoom * zoom2);
        start++;
        for (int i = start; i < MAX_SAMPLES; i++) {
            if (samples[i].time100us == 0) { break; }
            drawAvgData(hWnd, hdc, x0, y0, i, samples[gI(i - 4)].gyroZ, samples[gI(i - 3)].gyroZ, samples[gI(i - 2)].gyroZ, samples[gI(i - 1)].gyroZ, samples[gI(i)].gyroZ, MPU6050_DisplayAverage.gyroZ, TRUE, zoom * zoom2);
        }
    }
    SelectObject(hdc, hOldPen);
}

