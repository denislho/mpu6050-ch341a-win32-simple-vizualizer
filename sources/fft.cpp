#include "fft.h"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "framework.h"
#include "MPU6050.h"
#include "MpuDraw.h"

double lerp(double a, double b, double i)
{
    return a * (1 - i) + b * i;
}

enum {
    TEST_IMPULSE
    , TEST_DC
    , TEST_NYQUIST
    , TEST_HALF
    , TEST_SQUARE
    , TEST_SQUAREDOUBLE
    , TEST_SAW
    , TEST_NOISE
    , TEST_COMPLEXIMPULSE
    , TEST_COMPLEXDC
    , TEST_COMPLEXNOISE
    , TEST_BLANK
};

#define TEST TEST_IMPULSE

void init(double* in_real, double* in_imag, uint64_t size)
{
    for (uint64_t i = 0; i < size; i++)
    {
        switch (TEST)
        {
        case TEST_IMPULSE:
            in_real[i] = 0;
            in_imag[i] = 0;
            in_real[0] = 1;
            break;
        case TEST_DC:
            in_real[i] = 1;
            in_imag[i] = 0;
            break;
        case TEST_NYQUIST:
            in_real[i] = cos(M_PI * i);
            in_imag[i] = 0;
            break;
        case TEST_HALF:
            in_real[i] = cos(M_PI * i / 2);
            in_imag[i] = 0;
            break;
        case TEST_SQUARE:
            in_real[i] = (i < size / 2) ? 1 : -1;
            in_imag[i] = 0;
            break;
        case TEST_SQUAREDOUBLE:
            in_real[i] = (i % (size / 2) < size / 4) ? 1 : -1;
            in_imag[i] = 0;
            break;
        case TEST_SAW:
            in_real[i] = lerp(-1, 1, i / double(size - 1)); // subtract 1 from size to avoid DC from incorrect starting phase
            in_imag[i] = 0;
            break;
        case TEST_NOISE:
            in_real[i] = rand() % 1024 / 512.0 - 1;
            in_imag[i] = 0;
            break;
        case TEST_COMPLEXIMPULSE:
            in_real[i] = 0;
            in_imag[i] = 0;
            in_real[0] = 1;
            in_imag[0] = 1;
            break;
        case TEST_COMPLEXDC:
            in_real[i] = 1;
            in_imag[i] = 1;
            break;
        case TEST_COMPLEXNOISE:
            in_real[i] = rand() % 1024 / 512.0 - 1;
            in_imag[i] = rand() % 1024 / 512.0 - 1;
            break;
        case TEST_BLANK:
        default:
            in_real[i] = 0;
            in_imag[i] = 0;
        }
    }
}

// df = sampling frequency / N
// df = (1/sampling time) / N
uint32_t getFFTScaleMilliHertz(MPU6050_Measures* samples, int size)
{
    uint64_t dt100us = (samples[size - 1].time100us - samples[0].time100us); // total elapsed time
    uint64_t dtnanos = dt100us * 100 * 1000; // total elapsed time in ns
    uint64_t samplingns = dtnanos / size; // average sampling time in ns
    if (samplingns == 0) {
        return 1;
    }
    uint64_t df = (1000000000 / samplingns);
    df = df *1000 / size;
    if (df == 0) {
        return 1;
    }
    return df;
}

int doFFT(MPU6050_Measures *samples, int size, MPU6050_Measures* results, int channel, int fftWindowType)
{
    srand(time(NULL));
    double* in_real = (double*)malloc(sizeof(double) * size);
    double* in_imag = (double*)malloc(sizeof(double) * size);
    double* out_real = (double*)malloc(sizeof(double) * size);
    double* out_imag = (double*)malloc(sizeof(double) * size);
    //init(in_real, in_imag, size);

    for (int i = 0; i < size; i++) {
        switch (channel) {
        case 0: in_real[i] = (double)samples[i].accX; break;
        case 1: in_real[i] = (double)samples[i].accY; break;
        case 2: in_real[i] = (double)samples[i].accZ; break;
        case 3: in_real[i] = (double)samples[i].gyroX; break;
        case 4: in_real[i] = (double)samples[i].gyroY; break;
        case 5: in_real[i] = (double)samples[i].gyroZ; break;
        default: return 0;
        }
        in_imag [i] = 0.0;
        out_real[i] = 0.0;
        out_imag[i] = 0.0;
    }

    double fSize = (double)size;

    // apply windowing
    if (fftWindowType == 0) {
        // no window
    }
    else if (fftWindowType == 1) {
        // triangle
        for (int i = 0; i < size/2; i++) {
            double dt = (double)i;
            in_real[i] = in_real[i] * dt / fSize;
            in_real[size-1 - i] = in_real[size-1 - i] * dt / fSize;
        }
    }
    else if (fftWindowType == 2) {
        //Hann
        for (int i = 0; i < size; i++) {
            double dt = (double)i;
            in_real[i] = in_real[i] * (  0.5 - 0.5 * cos( 2*3.1415* dt / fSize) );
        }
    }
    else if (fftWindowType == 3) {
        //Hamming
        for (int i = 0; i < size; i++) {
            double dt = (double)i;
            in_real[i] = in_real[i] * (0.54 - 0.46 * cos(2 * 3.1415 * dt / fSize));
        }
    }
    else if (fftWindowType == 4) {
        //Blackman
        for (int i = 0; i < size; i++) {
            double dt = (double)i;
            in_real[i] = in_real[i] * (0.42 - 0.5 * cos(2 * 3.1415 * dt / fSize) + 0.08 * cos(4 * 3.1415 * dt / fSize) );
        }
    }


    fft(in_real, in_imag, size, out_real, out_imag);

    // end FFT display for real inputs early
    bool signal_was_real = (TEST < TEST_COMPLEXIMPULSE or TEST > TEST_COMPLEXNOISE);

    double factor;
    for (int i = 0; i < size; i++)
    {
        // boost nonunique frequencies for real inputs on display
        factor = (i > 0 and i < size / 2 and signal_was_real) ? 2 : 1;
        // end FFT display for real inputs early
        if (i >= size / 2 and signal_was_real)
            break;
    }

    for (int i = 0; i < size; i++) {
        switch (channel) {
        case 0: results[i].accX = 0; break;
        case 1: results[i].accY = 0; break;
        case 2: results[i].accZ = 0; break;
        case 3: results[i].gyroX = 0; break;
        case 4: results[i].gyroY = 0; break;
        case 5: results[i].gyroZ = 0; break;
        }
        results[i].time100us = i + 1; //FIXME : remove this +1
    }

    for (int i = 0; i < size/2; i++){
        double s = sqrt(out_real[i] * out_real[i] + out_imag[i] * out_imag[i]); // *factor;
        switch (channel) {
        case 0: results[i].accX = s; break;
        case 1: results[i].accY = s; break;
        case 2: results[i].accZ = s; break;
        case 3: results[i].gyroX = s; break;
        case 4: results[i].gyroY = s; break;
        case 5: results[i].gyroZ = s; break;
        }
        results[i].time100us = i+1; //FIXME : remove this +1
    }

    return 1;
}
