#include "MpuDraw.h"
#include <windowsx.h>
#include <stdint.h>
#include <cstdlib>
#include "framework.h"
#include "MPU6050.h"
#include "resource.h"
#include "USBIO.H"
#include <realtimeapiset.h>

extern MPU6050_Measures MPU6050_Samples[MAX_SAMPLES];
extern MPU6050_Measures MPU6050_Samples2[MAX_SAMPLES];
extern MPU6050_Measures MPU6050_Speed[MAX_SAMPLES];
extern MPU6050_Measures MPU6050_Speed2[MAX_SAMPLES];
extern MPU6050_Measures MPU6050_Position[MAX_SAMPLES];
extern MPU6050_Measures MPU6050_Position2[MAX_SAMPLES];
extern MPU6050_Measures MPU6050_LowLevel[MAX_SAMPLES];
extern MPU6050_Measures MPU6050_LowLevel2[MAX_SAMPLES];

extern int Displayed_Time[MAX_SAMPLES];

extern uint8_t mpu6050Address;
extern uint8_t mpu6050Address2;

extern bool realtimeMode;
extern int  realtimeIndex;

extern int realtimeAveraging;
extern int threadCurrentSampleIndex;
extern int filterByAverage;


void clearSample(MPU6050_Measures* oneMeasure)
{
    oneMeasure->accX = 0;
    oneMeasure->accY = 0;
    oneMeasure->accZ = 0;
    oneMeasure->gyroX = 0;
    oneMeasure->gyroY = 0;
    oneMeasure->gyroZ = 0;
    oneMeasure->time100us = 0;
}
/**
* Generate set of test data to see effects of integration
* and zoom on the screen
*/
bool testIntegral()
{
    for (int i = 0; i < MAX_SAMPLES; i++) {
        MPU6050_Samples[i].accX = i % 100 - 50;
        MPU6050_Samples[i].accY = (i / 100) % 2 == 0 ? -100 : 100;
        MPU6050_Samples[i].accZ = sin((double)i / 31.4) * 100.0;
        MPU6050_Samples[i].gyroX = i % 100;
        MPU6050_Samples[i].gyroY = i % 100 ? 10 : 0;
        MPU6050_Samples[i].gyroZ = sin(328.0 / ((double)i + 1) + 1) * 100;
        MPU6050_Samples[i].temperature = 20;
        MPU6050_Samples[i].time100us = i * 10 + 1; // one point every milli-seconde
    }
    for (int i = 0; i < MAX_SAMPLES; i++) {
        MPU6050_Samples2[i].accX = sin((double)i / 31.4) * 100.0 + sin(3 * (double)i / 31.4) * 50.0 + sin(7 * (double)i / 31.4) * 25.0 - 175;
        MPU6050_Samples2[i].accY = 1;
        MPU6050_Samples2[i].accZ = 1;
        MPU6050_Samples2[i].gyroX = 1;
        MPU6050_Samples2[i].gyroY = 1;
        MPU6050_Samples2[i].gyroZ = i > 400 && i < 500 ? 100 : -100;
        MPU6050_Samples2[i].temperature = 20;
        MPU6050_Samples2[i].time100us = i * 10 + 1; // one point every milli-seconde
    }
    return true;
}
/**
* Reload a CSV file generated previously, and load the values in samples
*/
bool reload(const char* fileName, MPU6050_Measures* samples, MPU6050_Measures* speed, MPU6050_Measures* position)
{
    bool res = FALSE;
    FILE* f;
    char buffer[800];
    int count = 0;

    f = nullptr;
    fopen_s(&f, fileName, "rt");
    if (f) {
        fgets(buffer, sizeof(buffer), f); // skip title
        for (int i = 0; i < MAX_SAMPLES; i++) {
            int fc = fscanf_s(f, "%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i\n",
                &samples->accX, &samples->accY, &samples->accZ, &samples->temperature,
                &samples->gyroX, &samples->gyroY, &samples->gyroZ, &samples->time100us,
                &speed->accX, &speed->accY, &speed->accZ, &speed->temperature,
                &speed->gyroX, &speed->gyroY, &speed->gyroZ, &speed->time100us,
                &position->accX, &position->accY, &position->accZ, &position->temperature,
                &position->gyroX, &position->gyroY, &position->gyroZ, &position->time100us
            );
            if (fc == EOF) {
                break;
            }
            count++;
            samples->time100us = i * 10 + 1; // Relative values are exported that start at 0
            speed->time100us = samples->time100us; // Relative values are exported that start at 0
            position->time100us = samples->time100us; // Relative values are exported that start at 0
            samples++;
            speed++;
            position++;
        }
        fclose(f);
    }
    res = TRUE;
    return res;
}

/**
* Save onle line of data that contains raw data, integrated data (speed), and double integrated data (position)
*/
bool saveSamplesBloc(const char* fileName, MPU6050_Measures* samples, MPU6050_Measures* speed, MPU6050_Measures* position)
{
    bool res = FALSE;
    FILE* f;
    char writeData[1024];
    memset(writeData, 0, sizeof(writeData));

    uint32_t start = samples->time100us;
    if (start == 0) {
        return FALSE;
    }

    if (mpu6050Address) {
        f = nullptr;
        fopen_s(&f, fileName, "wt");
        if (f) {
            fprintf(f, "AccX,AccY,AccZ,Temperature,GyroX,GyroY,GyroZ,Time 100us,");
            fprintf(f, "SpeedX,SpeedY,SpeedZ,Temperature,RotX,RotY,RotZ,Time 100us,");
            fprintf(f, "PosX,PosY,PosZ,Temperature,AngleX,AngleY,AngleZ,Time 100us\n");
            for (int i = 0; i < MAX_SAMPLES; i++) {
                fprintf(f, "%i,%i,%i,%i,%i,%i,%i,%i,", samples->accX, samples->accY, samples->accZ, samples->temperature,
                    samples->gyroX, samples->gyroY, samples->gyroZ, samples->time100us - start);
                samples++;
                fprintf(f, "%i,%i,%i,%i,%i,%i,%i,%i,", speed->accX, speed->accY, speed->accZ, speed->temperature,
                    speed->gyroX, speed->gyroY, speed->gyroZ, speed->time100us - start);
                speed++;
                fprintf(f, "%i,%i,%i,%i,%i,%i,%i,%i\n", position->accX, position->accY, position->accZ, position->temperature,
                    position->gyroX, position->gyroY, position->gyroZ, position->time100us - start);
                position++;
            }
            fflush(f);
            fclose(f);
        }
        res = TRUE;
    }
    return res;
}


/*
* Saves raw and integrated data into CSV file
*/
bool saveSamples(char* filename)
{
    char buffer[MAX_PATH];
    strcpy_s(buffer, sizeof(buffer), filename);
    char* ext = strstr(buffer, ".csv");
    if (!ext) {
        ext = strstr(buffer, ".CSV");
    }
    if (ext) {
        strcpy_s(ext, sizeof(buffer), "_68.csv");
    }
    else {
        strcat_s(buffer, sizeof(buffer), "_68.csv");
    }
    bool res1 = saveSamplesBloc(buffer, MPU6050_Samples, MPU6050_Speed, MPU6050_Position);

    strcpy_s(buffer, sizeof(buffer), filename);
    ext = strstr(buffer, ".csv");
    if (!ext) {
        ext = strstr(buffer, ".CSV");
    }
    if (ext) {
        strcpy_s(ext, sizeof(buffer), "_69.csv");
    }
    else {
        strcat_s(buffer, sizeof(buffer), "_69.csv");
    }
    bool res2 = saveSamplesBloc(buffer, MPU6050_Samples2, MPU6050_Speed2, MPU6050_Position2);
    return res1 || res2;
}
void copySample(MPU6050_Measures* src, MPU6050_Measures* dest)
{
    dest->accX = src->accX;
    dest->accY = src->accY;
    dest->accZ = src->accZ;
    dest->gyroX = src->gyroX;
    dest->gyroY = src->gyroY;
    dest->gyroZ = src->gyroZ;
    dest->temperature = src->temperature;
    dest->time100us = src->time100us;
}


/**
* Read values from MPU6050  and stores them in MPU6050_Samples[]
* In case of subsampling, this routine may take more than 30 seconds
* Write blocks of sampleCount at index startIndex.
* If average, then a set of subSample values are averaged,
* If not average then subSample are read, but only the first one is kept (to keep speed homogeneous)
*/
bool loadSamplingData(int subSample, int sampleCount, int startIndex, bool average)
{
    bool res = TRUE;
    //Get Tick Count is precise only at 10 to 16 ms.
    // So let's use a 100ns precise counter, but keep only 100 microseconds
    ULONGLONG interruptTimePreciseStart; 
    QueryInterruptTimePrecise(&interruptTimePreciseStart);
    ULONGLONG interruptTimePrecise;
    if (sampleCount < 1 || sampleCount > MAX_SAMPLES) {
        sampleCount = MAX_SAMPLES;
    }
    if (startIndex + sampleCount >= MAX_SAMPLES) {
        startIndex = 0;
    }
    for (int i = 0; i < sampleCount; i++) {
        // to simulate subsampling, sample at normal speed, 
        // but overwritte unwanted data...
        for (int j = 0; j < subSample; j++) {

            if (mpu6050Address != 0) {
                if (!MPU6050_I2CRead(mpu6050Address, &(MPU6050_LowLevel[j]))) {
                    res = FALSE;
                }

                QueryInterruptTimePrecise(&interruptTimePrecise);
                interruptTimePrecise = (interruptTimePrecise - interruptTimePreciseStart) / 1000;
                MPU6050_LowLevel[j].time100us = (uint32_t)(interruptTimePrecise) +1;
            }
            if (mpu6050Address2 != 0) {
                if (!MPU6050_I2CRead(mpu6050Address2, &(MPU6050_LowLevel2[j]))) {
                    res = FALSE;
                }

                QueryInterruptTimePrecise(&interruptTimePrecise);
                interruptTimePrecise = (interruptTimePrecise - interruptTimePreciseStart) / 1000;
                MPU6050_LowLevel[j].time100us = (uint32_t)(interruptTimePrecise) +1;
            }
        }
        if (subSample == 1 || !average) {
            if (mpu6050Address) { copySample(&(MPU6050_LowLevel[0]), &(MPU6050_Samples[startIndex + i])); }
            if (mpu6050Address2) { copySample(&(MPU6050_LowLevel2[0]), &(MPU6050_Samples2[startIndex + i])); }
        }
        else {
            if (mpu6050Address2) { computeAverage(MPU6050_LowLevel, &(MPU6050_Samples[startIndex + i]), subSample); }
            if (mpu6050Address2) { computeAverage(MPU6050_LowLevel2, &(MPU6050_Samples2[startIndex + i]), subSample); }
        }
    }
    if (startIndex + sampleCount < MAX_SAMPLES) {
        MPU6050_Samples[startIndex + sampleCount].time100us = 0;
        MPU6050_Samples2[startIndex + sampleCount].time100us = 0;
    }
    return res;
}

DWORD WINAPI SamplingThreadFunction(LPVOID lpParam)
{
    //    PMYDATA pDataArray;

    while (realtimeMode) {
        for (threadCurrentSampleIndex = 0; threadCurrentSampleIndex < MAX_SAMPLES; threadCurrentSampleIndex++) {
            loadSamplingData(realtimeAveraging, 1, threadCurrentSampleIndex, FALSE);
            if (!realtimeMode) {
                for (; threadCurrentSampleIndex < MAX_SAMPLES; threadCurrentSampleIndex++) {
                    clearSample(&(MPU6050_Samples[threadCurrentSampleIndex]));
                    clearSample(&(MPU6050_Samples2[threadCurrentSampleIndex]));
                }
                break;
            }
        }
    }

    return 0;
}
// Not used yet
DWORD WINAPI SingleSamplingThreadFunction(LPVOID lpParam)
{
    for (threadCurrentSampleIndex = 0; threadCurrentSampleIndex < MAX_SAMPLES; threadCurrentSampleIndex++) {
        loadSamplingData(realtimeAveraging, MAX_SAMPLES, threadCurrentSampleIndex, FALSE);
    }
    return 0;
}


// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved

//
//   FUNCTION: CaptureAnImage(HWND hWnd)
//
//   PURPOSE: Captures a screenshot into a window ,and then saves it in a .bmp file.
//
//   COMMENTS: 
//
//      Note: This function attempts to create a file fileName 
//
// Modified to grab only this application' screen
// generate filename based on fileName.type.bmp

int CaptureAnImage(HWND hWnd, LPCWSTR fileName, LPCWSTR type)
{
    HDC hdcWindow;
    HDC hdcMemDC = NULL;
    HBITMAP hbmWindow = NULL;
    BITMAP bmpScreen;
    DWORD dwBytesWritten = 0;
    DWORD dwSizeofDIB = 0;
    HANDLE hFile = NULL;
    char* lpbitmap = NULL;
    HANDLE hDIB = NULL;
    DWORD dwBmpSize = 0;
    WCHAR bmpFileName[MAX_PATH];

    wcscpy_s(bmpFileName, MAX_PATH/2, fileName );
    wcscat_s(bmpFileName, MAX_PATH/2, type);
    wcscat_s(bmpFileName, MAX_PATH/2, L".bmp");

    // Retrieve the handle to a display device context for the client 
    // area of the window. 
    hdcWindow = GetDC(hWnd);

    // Create a compatible DC, which is used in a BitBlt from the window DC.
    hdcMemDC = CreateCompatibleDC(hdcWindow);

    if (!hdcMemDC)
    {
        MessageBox(hWnd, L"CreateCompatibleDC has failed", L"Failed", MB_OK);
        goto done;
    }

    // Get the client area for size calculation.
    RECT rcClient;
    GetClientRect(hWnd, &rcClient);


    // Create a compatible bitmap from the Window DC.
    hbmWindow = CreateCompatibleBitmap(hdcWindow, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);

    if (!hbmWindow)
    {
        MessageBox(hWnd, L"CreateCompatibleBitmap Failed", L"Failed", MB_OK);
        goto done;
    }

    // Select the compatible bitmap into the compatible memory DC.
    SelectObject(hdcMemDC, hbmWindow);

    // Bit block transfer into our compatible memory DC.
    if (!BitBlt(hdcMemDC,
        0, 0,
        rcClient.right - rcClient.left, rcClient.bottom - rcClient.top,
        hdcWindow,
        0, 0,
        SRCCOPY))
    {
        MessageBox(hWnd, L"BitBlt has failed", L"Failed", MB_OK);
        goto done;
    }

    // Get the BITMAP from the HBITMAP.
    GetObject(hbmWindow, sizeof(BITMAP), &bmpScreen);

    BITMAPFILEHEADER   bmfHeader;
    BITMAPINFOHEADER   bi;

    bi.biSize = sizeof(BITMAPINFOHEADER);
    bi.biWidth = bmpScreen.bmWidth;
    bi.biHeight = bmpScreen.bmHeight;
    bi.biPlanes = 1;
    bi.biBitCount = 32;
    bi.biCompression = BI_RGB;
    bi.biSizeImage = 0;
    bi.biXPelsPerMeter = 0;
    bi.biYPelsPerMeter = 0;
    bi.biClrUsed = 0;
    bi.biClrImportant = 0;

    dwBmpSize = ((bmpScreen.bmWidth * bi.biBitCount + 31) / 32) * 4 * bmpScreen.bmHeight;

    // Starting with 32-bit Windows, GlobalAlloc and LocalAlloc are implemented as wrapper functions that 
    // call HeapAlloc using a handle to the process's default heap. Therefore, GlobalAlloc and LocalAlloc 
    // have greater overhead than HeapAlloc.
    hDIB = GlobalAlloc(GHND, dwBmpSize);
    lpbitmap = (char*)GlobalLock(hDIB);

    // Gets the "bits" from the bitmap, and copies them into a buffer 
    // that's pointed to by lpbitmap.
    GetDIBits(hdcWindow, hbmWindow, 0,
        (UINT)bmpScreen.bmHeight,
        lpbitmap,
        (BITMAPINFO*)&bi, DIB_RGB_COLORS);

    // A file is created, this is where we will save the screen capture.
    hFile = CreateFile(bmpFileName,
        GENERIC_WRITE,
        0,
        NULL,
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL, NULL);

    // Add the size of the headers to the size of the bitmap to get the total file size.
    dwSizeofDIB = dwBmpSize + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

    // Offset to where the actual bitmap bits start.
    bmfHeader.bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER) + (DWORD)sizeof(BITMAPINFOHEADER);

    // Size of the file.
    bmfHeader.bfSize = dwSizeofDIB;

    // bfType must always be BM for Bitmaps.
    bmfHeader.bfType = 0x4D42; // BM.

    WriteFile(hFile, (LPSTR)&bmfHeader, sizeof(BITMAPFILEHEADER), &dwBytesWritten, NULL);
    WriteFile(hFile, (LPSTR)&bi, sizeof(BITMAPINFOHEADER), &dwBytesWritten, NULL);
    WriteFile(hFile, (LPSTR)lpbitmap, dwBmpSize, &dwBytesWritten, NULL);

    // Unlock and Free the DIB from the heap.
    GlobalUnlock(hDIB);
    GlobalFree(hDIB);

    // Close the handle for the file that was created.
    CloseHandle(hFile);

    // Clean up.
done:
    DeleteObject(hbmWindow);
    DeleteObject(hdcMemDC);
    ReleaseDC(hWnd, hdcWindow);

    return 0;
}

