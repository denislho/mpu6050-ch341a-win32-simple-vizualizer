// MPU6050.cpp : Définit le point d'entrée de l'application.
//
#include <windowsx.h>
#include <stdint.h>
#include <cstdlib>
#include "framework.h"
#include "MPU6050.h"
#include "resource.h"
#include "USBIO.H"
#include <realtimeapiset.h>
#include "MpuDraw.h"
#include <commdlg.h>
#include <winuser.h>
#include <tchar.h>
#include <strsafe.h>

HANDLE hThreadSampling = nullptr;
DWORD  dwSamplingThreadId = 0;
int latestDisplaySampleIndex = -1;
int threadCurrentSampleIndex = -1;
extern DWORD WINAPI SamplingThreadFunction(LPVOID lpParam);
extern DWORD WINAPI SingleSamplingThreadFunction(LPVOID lpParam);

int doFFT(MPU6050_Measures* samples, int size, MPU6050_Measures* results, int channel, int fftWindowType);
uint32_t getFFTScaleMilliHertz(MPU6050_Measures* samples, int size);
int CaptureAnImage(HWND hWnd, LPCWSTR fileName, LPCWSTR type);


HWND hScales = nullptr;

// select what to draw
bool showMPU6050_1_Acc  = TRUE;
bool showMPU6050_2_Acc  = TRUE;
bool showMPU6050_1_Gyro = TRUE;
bool showMPU6050_2_Gyro = TRUE;

bool realtimeMode = FALSE;
bool realtimeDrawScale = FALSE;

int filterByAverage = 0;
int realtimeIndex = 0;
int realtimeAveraging = 1;

int userNumberInput; // to get an input value from user
int usernumberInputTarget = -1; // integer to modify at the end of typing

// Record point when mouse left button is pressed
int MouseDownX = 0;
int MouseDownY = 0;
// Record point when mouse left button is released
int MouseUpX = 0;
int MouseUpY = 0;

// Record point when mouse left button is pressed while  <SHIFT> is pressed
int MouseDownShiftYLeft  = 0;
int MouseDownShiftYRight = 0;

// position of axles
int xAxle1YPos = 0;
int xAxle2YPos = 0;

// settings sent to MPU6050
int currentAccelerationRatio = 1;
int currentGyroscopeRatio = 1;

int xZoom = 1;
int xCenter = 0;
int index100usAtCenter = 0;

uint32_t xLegendScale = 0;
uint32_t xLegendScale2 = 0;

int splitY = 0; // separate X-axle between Acc1 and Acc2

int zoom = ZOOM_UNITY;
int zoom2 = 1; // multiplier to zoom axle 2 more than axle 1

uint8_t mpu6050Address  = 0;
uint8_t mpu6050Address2 = 0;

bool centerDrawingToZero = FALSE;

// Identify the USB->I2C device used
UINT mIndex = -1;
/**
* Sampling data storage
*/
extern MPU6050_Measures MPU6050_Samples  [MAX_SAMPLES];
extern MPU6050_Measures MPU6050_Samples2 [MAX_SAMPLES];
extern MPU6050_Measures MPU6050_Samples2 [MAX_SAMPLES];
extern MPU6050_Measures MPU6050_Speed    [MAX_SAMPLES];
extern MPU6050_Measures MPU6050_Speed2   [MAX_SAMPLES];
extern MPU6050_Measures MPU6050_Position [MAX_SAMPLES];
extern MPU6050_Measures MPU6050_Position2[MAX_SAMPLES];
extern MPU6050_Measures MPU6050_FFT      [MAX_SAMPLES];
extern MPU6050_Measures MPU6050_FFT2     [MAX_SAMPLES];


extern MPU6050_Measures* MPU6050_DisplayData;
extern MPU6050_Measures* MPU6050_DisplayData2;
extern MPU6050_Measures MPU6050_DisplayAverage;
extern MPU6050_Measures MPU6050_DisplayAverage2;

HPEN hPenAX = CreatePen(PS_SOLID, 1, RGB(255, 64, 64));
HPEN hPenAY = CreatePen(PS_SOLID, 1, RGB( 0, 255,  0));
HPEN hPenAZ = CreatePen(PS_SOLID, 1, RGB(64, 64, 255));
HPEN hPenGX = CreatePen(PS_SOLID, 1, RGB(255, 128, 128));
HPEN hPenGY = CreatePen(PS_SOLID, 1, RGB( 64, 255,  64));
HPEN hPenGZ = CreatePen(PS_SOLID, 1, RGB(128, 128, 255));


HWND  cbA1X, cbA1Y, cbA1Z, cbG1X, cbG1Y, cbG1Z;
HWND  cbA2X, cbA2Y, cbA2Z, cbG2X, cbG2Y, cbG2Z;




// Variables globales :
HINSTANCE hInst;                                // instance actuelle
WCHAR szTitle[MAX_LOADSTRING];                  // Texte de la barre de titre
WCHAR szWindowClass[MAX_LOADSTRING];            // nom de la classe de fenêtre principale
HWND mainHWnd;

// Déclarations anticipées des fonctions incluses dans ce module de code :
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    memset(MPU6050_Samples,   0, sizeof(MPU6050_Samples   ));
    memset(MPU6050_Samples2,  0, sizeof(MPU6050_Samples2  ));
    memset(MPU6050_Speed,     0, sizeof(MPU6050_Speed     ));
    memset(MPU6050_Speed2,    0, sizeof(MPU6050_Speed2    ));
    memset(MPU6050_Position,  0, sizeof(MPU6050_Position  ));
    memset(MPU6050_Position2, 0, sizeof(MPU6050_Position2 ));

    // TODO: Placez le code ici.

    // Initialise les chaînes globales
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MPU6050, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Effectue l'initialisation de l'application :
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MPU6050));

    MSG msg;


    // Boucle de messages principale :
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FONCTION : MyRegisterClass()
//
//  OBJECTIF : Inscrit la classe de fenêtre.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MPU6050));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MPU6050);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FONCTION : InitInstance(HINSTANCE, int)
//
//   OBJECTIF : enregistre le handle d'instance et crée une fenêtre principale
//
//   COMMENTAIRES :
//
//        Dans cette fonction, nous enregistrons le handle de l'instance dans une variable globale, puis
//        nous créons et affichons la fenêtre principale du programme.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Stocke le handle d'instance dans la variable globale

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }
   mainHWnd = hWnd;
   // Create a kind of groupbox
   hScales = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CHILD,
           0, 0, 800, 200, hWnd, nullptr, hInstance, nullptr);

   cbA1X = CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"AX", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 300, 10, 50, 20, hScales, (HMENU)ID_CHANNEL_A1X, hInst, NULL);
   cbA1Y = CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"AY", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 350, 10, 50, 20, hScales, (HMENU)ID_CHANNEL_A1Y, hInst, NULL);
   cbA1Z = CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"AZ", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 400, 10, 50, 20, hScales, (HMENU)ID_CHANNEL_A1Z, hInst, NULL);
   cbG1X = CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"GX", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 450, 10, 50, 20, hScales, (HMENU)ID_CHANNEL_G1X, hInst, NULL);
   cbG1Y = CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"GY", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 500, 10, 50, 20, hScales, (HMENU)ID_CHANNEL_G1Y, hInst, NULL);
   cbG1Z = CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"GZ", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 550, 10, 50, 20, hScales, (HMENU)ID_CHANNEL_G1Z, hInst, NULL);

   cbA2X = CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"AX", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 300, 40, 50, 20, hScales, (HMENU)ID_CHANNEL_A2X, hInst, NULL);
   cbA2Y = CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"AY", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 350, 40, 50, 20, hScales, (HMENU)ID_CHANNEL_A2Y, hInst, NULL);
   cbA2Z = CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"AZ", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 400, 40, 50, 20, hScales, (HMENU)ID_CHANNEL_A2Z, hInst, NULL);
   cbG2X = CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"GX", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 450, 40, 50, 20, hScales, (HMENU)ID_CHANNEL_G2X, hInst, NULL);
   cbG2Y = CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"GY", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 500, 40, 50, 20, hScales, (HMENU)ID_CHANNEL_G2Y, hInst, NULL);
   cbG2Z = CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"GZ", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 550, 40, 50, 20, hScales, (HMENU)ID_CHANNEL_G2Z, hInst, NULL);

   CheckDlgButton(hScales, ID_CHANNEL_A1X, BST_CHECKED);
   CheckDlgButton(hScales, ID_CHANNEL_A1Y, BST_CHECKED);
   CheckDlgButton(hScales, ID_CHANNEL_A1Z, BST_CHECKED);
   CheckDlgButton(hScales, ID_CHANNEL_G1X, BST_CHECKED);
   CheckDlgButton(hScales, ID_CHANNEL_G1Y, BST_CHECKED);
   CheckDlgButton(hScales, ID_CHANNEL_G1Z, BST_CHECKED);

   CheckDlgButton(hScales, ID_CHANNEL_A2X, BST_CHECKED);
   CheckDlgButton(hScales, ID_CHANNEL_A2Y, BST_CHECKED);
   CheckDlgButton(hScales, ID_CHANNEL_A2Z, BST_CHECKED);
   CheckDlgButton(hScales, ID_CHANNEL_G2X, BST_CHECKED);
   CheckDlgButton(hScales, ID_CHANNEL_G2Y, BST_CHECKED);
   CheckDlgButton(hScales, ID_CHANNEL_G2Z, BST_CHECKED);

   CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"MPU6050 0x68", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,  10, 3, 120, 20, hScales, (HMENU)ID_CHANNEL_DETAILS_1, hInst, NULL);
   CreateWindowEx(WS_EX_WINDOWEDGE, L"BUTTON", L"MPU6050 0x69", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, 610, 3, 120, 20, hScales, (HMENU)ID_CHANNEL_DETAILS_2, hInst, NULL);
   CheckDlgButton(hWnd, ID_CHANNEL_DETAILS_1, BST_CHECKED);
   CheckDlgButton(hWnd, ID_CHANNEL_DETAILS_2, BST_CHECKED);

   checkMenu(hWnd, ID_DRAW_MEASURE, TRUE);
   checkMenu(hWnd, ID_FILTER_NO_CENTER_0, TRUE);
   checkMenu(hWnd, ID_FILTER_RAW, TRUE);  
   

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

bool checkMenu(HWND hWnd, UINT message, bool check)
{
    MENUITEMINFO menuItem;
    HMENU hmenu = GetMenu(hWnd);

    memset(&menuItem, 0, sizeof(MENUITEMINFO));
    menuItem.cbSize = sizeof(MENUITEMINFO);

    
    if (GetMenuItemInfo(hmenu, message, FALSE, &menuItem)) {
        if (check) {
            menuItem.fState = MFS_CHECKED;
        }
        else {
            menuItem.fState = MFS_UNCHECKED;
        }
        menuItem.fMask = MIIM_STATE;
        bool b = SetMenuItemInfo(hmenu, message, FALSE, &menuItem);
        if (b) {
            //MessageBox(hWnd, _T("Menu checked"), _T("Success"), MB_OK);
        }
        else {
            MessageBox(hWnd, _T("Failed to check Menu"), _T("Error"), MB_OK);
        }
        return b;
    }
    else {
        MessageBox(hWnd, _T("Failed while trying to read Menu"), _T("Error"), MB_OK);
        return FALSE;
    }
}

bool save(HWND hWnd)
{
    bool b;
    OPENFILENAME ofn;       // common dialog box structure
    WCHAR szFile[MAX_PATH];       // buffer for file name

    // Initialize OPENFILENAME
    ZeroMemory(&ofn, sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = hWnd;
    ofn.lpstrFile = (LPWSTR)szFile;
    //
    // Set lpstrFile[0] to '\0' so that GetOpenFileName does not 
    // use the contents of szFile to initialize itself.
    //
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = sizeof(szFile);
    ofn.lpstrFilter = L"All\0*.*\0Text\0*.CSV\0";
    ofn.nFilterIndex = 1;
    ofn.lpstrFileTitle = NULL;
    ofn.nMaxFileTitle = 0;
    ofn.lpstrInitialDir = NULL;
    ofn.Flags = OFN_PATHMUSTEXIST; // | OFN_FILEMUSTEXIST;

    // Display the Open dialog box. 

    if (GetOpenFileName(&ofn)) {
        char fn[2*MAX_PATH];
        size_t l;
        int length = wcstombs_s(&l, fn, MAX_PATH, szFile, MAX_PATH);
        b = saveSamples(fn);
        const WCHAR* type;
        if (MPU6050_DisplayData == MPU6050_Samples) {
            type = L"_samples";
        }else if (MPU6050_DisplayData == MPU6050_Speed) {
            type = L"_speed";
        }else if (MPU6050_DisplayData == MPU6050_Position) {
            type = L"_position";
        }else if (MPU6050_DisplayData == MPU6050_FFT) {
            type = L"_fft";
        }
        else {
            type = L"_";
        }
        CaptureAnImage(hWnd, ofn.lpstrFile, type);
    }
    else {
        b = saveSamples((char*)"data");
    }
    if (b) {
        MessageBox(hWnd, _T("Data saved"), _T("Information"), MB_OK);
    }
    else {
        MessageBox(hWnd, _T("Failed to save data."), _T("Error"), MB_OK);
    }
    return b;
}
bool reload(HWND hWnd)
{
    bool b1, b2;
    OPENFILENAME ofn;       // common dialog box structure
    WCHAR szFile[MAX_PATH];       // buffer for file name

    // Initialize OPENFILENAME
    ZeroMemory(&ofn, sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = hWnd;
    ofn.lpstrFile = (LPWSTR)szFile;
    //
    // Set lpstrFile[0] to '\0' so that GetOpenFileName does not 
    // use the contents of szFile to initialize itself.
    //
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = sizeof(szFile);
    ofn.lpstrFilter = L"All\0*.*\0Text\0*.CSV\0";
    ofn.nFilterIndex = 1;
    ofn.lpstrFileTitle = NULL;
    ofn.nMaxFileTitle = 0;
    ofn.lpstrInitialDir = NULL;
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

    // Display the Open dialog box. 

    if (GetOpenFileName(&ofn)) {
        char fn[2*MAX_PATH];
        size_t l;
        int length = wcstombs_s(&l, fn, MAX_PATH, szFile, MAX_PATH);

        memset(MPU6050_Samples, 0, sizeof(MPU6050_Samples));
        memset(MPU6050_Samples2, 0, sizeof(MPU6050_Samples2));
        memset(MPU6050_Speed, 0, sizeof(MPU6050_Speed));
        memset(MPU6050_Speed2, 0, sizeof(MPU6050_Speed2));
        memset(MPU6050_Position, 0, sizeof(MPU6050_Position));
        memset(MPU6050_Position2, 0, sizeof(MPU6050_Position2));

        b1 = reload(fn, MPU6050_Samples, MPU6050_Speed, MPU6050_Position);
        char* ptr68 = strstr(fn, "68");
        if (ptr68) {
            *(ptr68 + 1) = (char)"9";
            b2 = reload(fn, MPU6050_Samples2, MPU6050_Speed2, MPU6050_Position2);
        }
        else {
            b2 = FALSE;
        }
    }
    else {
        b1 = FALSE;
    }
    if (b1 && b2) {
        MessageBox(hWnd, _T("Data loaded"), _T("Information"), MB_OK);
    }
    else {
        MessageBox(hWnd, _T("Failed to load data."), _T("Error"), MB_OK);
    }
    return b1 && b2;
}

void ErrorHandler(LPTSTR lpszFunction)
{
    // Retrieve the system error message for the last-error code.

    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError();

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR)&lpMsgBuf,
        0, NULL);

    // Display the error message.

    lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
        (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
    StringCchPrintf((LPTSTR)lpDisplayBuf,
        LocalSize(lpDisplayBuf) / sizeof(TCHAR),
        TEXT("%s failed with error %d: %s"),
        lpszFunction, dw, lpMsgBuf);
    MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

    // Free error-handling buffer allocations.

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
}

void resetMenuRead(HWND hWnd)
{
    checkMenu(hWnd, ID_FICHIER_READ_1, FALSE);
    checkMenu(hWnd, ID_FICHIER_READ_2, FALSE);
    checkMenu(hWnd, ID_FICHIER_READ_4, FALSE);
    checkMenu(hWnd, ID_FICHIER_READ_8, FALSE);
    checkMenu(hWnd, ID_FICHIER_READ_16, FALSE);
    checkMenu(hWnd, ID_FICHIER_READ_32, FALSE);
    checkMenu(hWnd, ID_FICHIER_READ_1, FALSE);
    checkMenu(hWnd, ID_FICHIER_READ_AVG_2, FALSE);
    checkMenu(hWnd, ID_FICHIER_READ_AVG_4, FALSE);
    checkMenu(hWnd, ID_FICHIER_READ_AVG_8, FALSE);
    checkMenu(hWnd, ID_FICHIER_READ_AVG_16, FALSE);
    checkMenu(hWnd, ID_FICHIER_READ_AVG_32, FALSE);
    checkMenu(hWnd, ID_FICHIER_REALTIME_START_1, FALSE);
    checkMenu(hWnd, ID_FICHIER_REALTIME_START_5, FALSE);
    checkMenu(hWnd, ID_FICHIER_REALTIME_START_10, FALSE);
    checkMenu(hWnd, ID_FICHIER_REALTIME_START_20, FALSE);
    checkMenu(hWnd, ID_FICHIER_REALTIME_START_50, FALSE);
    checkMenu(hWnd, ID_FICHIER_REALTIME_START_100, FALSE);
}

void drawInput(HDC hdc)
{
    char* name = NULL;
    if (usernumberInputTarget == 0) {
        name = (char*)" Zoom Y =";
    }
    if (name) {
        drawStringAndInt(hdc, name, userNumberInput, 100, 50);
    }
}

void changeXZoom(bool increase)
{
    if (increase) {
        xZoom = xZoom * 2;
        if (xZoom > 128) {
            xZoom = 128;
        }
    }
    else {
        xZoom = xZoom / 2;
        if (xZoom < 1) {
            xZoom = 1;
        }
    }
}
void changeYzoom(bool increase)
{
    if (increase) {
        if (zoom < 5) {
            zoom++;
        }
        else {
            zoom = zoom * 130;
            zoom = zoom / 100;
        }
    }
    else {
        zoom = zoom * 100;
        zoom = zoom / 130;
        if (zoom < 1) {
            zoom = 1;
        }
    }
    // try to round zoom value
    if (zoom > 10000) {
        zoom = round((float)zoom / 1000.0);
        zoom = zoom * 1000;
    }else if (zoom > 1000) {
        zoom = round( (float)zoom / 100.0);
        zoom = zoom * 100;
    }
    else if (zoom > 100) {
        zoom = round((float)zoom / 10.0);
        zoom = zoom * 10;
    }
}

bool connectMPU6050(HWND hWnd, int message, int wmId)
{
    bool b1 = FALSE;
    if (wmId == ID_FICHIER_MPU60500_0) {
        mpu6050Address = 0x68;
        b1 = MPU6050_I2C_Init(mpu6050Address);
        //MessageBox(hWnd, _T("Connecting to 0x68"), _T("Information"), MB_OK);
    }
    else {
        mpu6050Address2 = 0x69;
        b1 = MPU6050_I2C_Init(mpu6050Address2);
        //MessageBox(hWnd, _T("Connecting to 0x69"), _T("Information"), MB_OK);
    }
    if (b1) {
        checkMenu(hWnd, wmId, b1);
    }
    return b1;
}
bool connectToUsb(HWND hWnd, int message, int wmId)
{
    if (LoadLibrary(_T("USBIOX.DLL")) == NULL) {
        MessageBox(hWnd, _T("Failed to load library USBIOX.DLL"), _T("Error"), MB_OK);
    }
    else {
        printf("USBIO_OpenDevice: 0\n");
    }
    mIndex = 0; // First USB device
    if (USBIO_OpenDevice(mIndex) == INVALID_HANDLE_VALUE)
    {
        MessageBox(hWnd, _T("Failed to open USA->I2C device"), _T("Error"), MB_OK);
        checkMenu(hWnd, message, FALSE);
        return FALSE;
    }
    else {
        //SET FREQUENCY of SCL:  0x80=20Khz  0x81=100Khz 0x82=400Khz 0x83=750Khz
        if (!USBIO_SetStream(mIndex, 0x82)) {
            MessageBox(hWnd, _T("Failed to set I2C speed."), _T("Error"), MB_OK);
            checkMenu(hWnd, wmId, FALSE);
            return FALSE;
        }
        else {
            checkMenu(hWnd, wmId, TRUE);
            MessageBox(hWnd, _T("Connected"), _T("Information"), MB_OK);
            return TRUE;
        }
    }
}
//
//  FONCTION : WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  OBJECTIF : Traite les messages pour la fenêtre principale.
//
//  WM_COMMAND  - traite le menu de l'application
//  WM_PAINT    - Dessine la fenêtre principale
//  WM_DESTROY  - génère un message d'arrêt et retourne
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    RECT detailsRect = { 0, 0,200,130 };
    int dx, dt;
    bool b1, b2;

    // Simulate mouse wheel from buttons and <CTRL>
    // Does not work for the moment !!!!
    if (message == WM_LBUTTONDOWN || message == WM_RBUTTONDOWN) {
        if (wParam & MK_CONTROL) {
            // simulate mouse wheel
            if (message == WM_LBUTTONDOWN) {
                wParam = 0x0780000 | (message & MK_SHIFT);
            }
            else {
                wParam = 0xFF880000 | (message & MK_SHIFT);
            }
            
            if (GetAsyncKeyState(VK_LBUTTON) && GetAsyncKeyState(VK_RBUTTON)) {
                message = WM_MBUTTONDOWN;
            }
            else {
                message = WM_MOUSEWHEEL;
            }
            int w = GET_WHEEL_DELTA_WPARAM(wParam);
        }
    }
    HMENU hmenu = GetMenu(hWnd);
    switch (message)
    {
    case WM_KEYDOWN:
        if (wParam == VK_ESCAPE) {
            //cacel input grabbing
            usernumberInputTarget = -1;
            InvalidateRect(hWnd, NULL, TRUE);
        }
        else if (wParam == VK_RETURN || wParam == VK_EXECUTE) {
            switch (usernumberInputTarget) {
            case 0: 
                zoom = userNumberInput>1 ? userNumberInput : 1; 
                usernumberInputTarget = -1;
                InvalidateRect(hWnd, NULL, TRUE); break;
                break;
            }
        }
        else if (wParam >= 0x30 && wParam <= 0x39) {
            userNumberInput = userNumberInput * 10 + (wParam - 0x30); 
            InvalidateRect(hWnd, NULL, TRUE); break;
        }
        else if (wParam >= 0x60 && wParam <= 0x69) {
            userNumberInput = userNumberInput * 10 + (wParam - 0x60);
            InvalidateRect(hWnd, NULL, TRUE); break;
        }
        else if (wParam >= 0x96 && wParam <= 0x100) {
            userNumberInput = userNumberInput * 10 + (wParam - 0x96);
            InvalidateRect(hWnd, NULL, TRUE); break;
        }
        else if (wParam == VK_HOME) {
            zoom = zoomAll();
            InvalidateRect(hWnd, NULL, TRUE);
        }
        else if ( (wParam & 0x00FF) == VK_PRIOR) {
            changeYzoom(TRUE);
            InvalidateRect(hWnd, NULL, TRUE);
        }
        else if ((wParam & 0x00FF) == VK_NEXT) {
            changeYzoom(FALSE);
            InvalidateRect(hWnd, NULL, TRUE);
        }
        else if ((wParam & 0x00FF) == VK_ADD) {
            changeXZoom(TRUE);
            InvalidateRect(hWnd, NULL, TRUE);
        }
        else if ((wParam & 0x00FF) == VK_SUBTRACT) {
            changeXZoom(FALSE);
            InvalidateRect(hWnd, NULL, TRUE);
        }
        else if ((wParam & 0x00FF) == VK_LEFT) {
            xCenter -= 50;
            if (xCenter < 0) { xCenter = 0; }
            index100usAtCenter = xCenter;
            InvalidateRect(hWnd, NULL, TRUE);
        }
        else if ((wParam & 0x00FF) == VK_RIGHT) {
            xCenter += 50;
            if (xCenter > MAX_SAMPLES-128) { xCenter = MAX_SAMPLES-128; }
            index100usAtCenter = xCenter;
            InvalidateRect(hWnd, NULL, TRUE);
        }
        else {
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
        break;
    case WM_MBUTTONDOWN:
        zoom = zoomAll();
        InvalidateRect(hWnd, NULL, TRUE);
        break;
    case WM_LBUTTONDOWN:
        
        if (wParam & MK_SHIFT) {
            MouseDownShiftYLeft = GET_Y_LPARAM(lParam);
        }
        else {
            MouseDownX = GET_X_LPARAM(lParam);
            MouseDownY = GET_Y_LPARAM(lParam);
            MouseUpX = 0;
            MouseUpY = 0;
        }
        //InvalidateRect(hWnd, &detailsRect, TRUE);
        InvalidateRect(hWnd, NULL, TRUE);
        break;
    case WM_RBUTTONDOWN:
        if (wParam & MK_SHIFT) {
            MouseDownShiftYRight = GET_Y_LPARAM(lParam);
        }
        else {
            MouseUpX = GET_X_LPARAM(lParam);
            MouseUpY = GET_Y_LPARAM(lParam);
        }
        //InvalidateRect(hWnd, &detailsRect, TRUE);
        InvalidateRect(hWnd, NULL, TRUE);
        break;

     case WM_MOUSEWHEEL:
         if (wParam & MK_SHIFT) {
             //zoom X and grab XCenter
             if (xZoom == 1) {
                 xCenter = GET_X_LPARAM(lParam);
                 index100usAtCenter = xCenter; // initialize with absolute value
             }
             changeXZoom(GET_WHEEL_DELTA_WPARAM(wParam) > 0);
         }
         else {
             // zoom Y
             changeYzoom(GET_WHEEL_DELTA_WPARAM(wParam) > 0);
         }
         InvalidateRect(hWnd, NULL, TRUE);
         break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Analyse les sélections de menu :
            switch (wmId)
            {
            case ID_REFRESH:
                InvalidateRect(hWnd, NULL, TRUE);
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            case ID_FICHIER_MPU60500_0:
            case ID_FICHIER_MPU60500_1:
                connectMPU6050(hWnd, message, wmId);
                break;
            case ID_MPU6050_ACC_2G:
            case ID_MPU6050_ACC_4G:
            case ID_MPU6050_ACC_8G:
            case ID_MPU6050_ACC_16G:
                b1 = MPU6050_I2C_SetScale(mpu6050Address, MPU6050_REG_ACCEL_CONFIG, wmId - ID_MPU6050_ACC_2G);
                b2 = MPU6050_I2C_SetScale(mpu6050Address2, MPU6050_REG_ACCEL_CONFIG, wmId - ID_MPU6050_ACC_2G);
                checkMenu(hWnd, ID_MPU6050_ACC_2G, FALSE);
                checkMenu(hWnd, ID_MPU6050_ACC_4G, FALSE);
                checkMenu(hWnd, ID_MPU6050_ACC_8G, FALSE);
                checkMenu(hWnd, ID_MPU6050_ACC_16G, FALSE);
                checkMenu(hWnd, wmId, b1 && b2);
                currentAccelerationRatio = (wmId - ID_MPU6050_ACC_2G + 1) * 2;
                break;
            case ID_MPU6050_GYRO_250:
            case ID_MPU6050_GYRO_500:
            case ID_MPU6050_GYRO_1000:
            case ID_MPU6050_GYRO_2000:
                checkMenu(hWnd, ID_MPU6050_GYRO_250, FALSE);
                checkMenu(hWnd, ID_MPU6050_GYRO_500, FALSE);
                checkMenu(hWnd, ID_MPU6050_GYRO_1000, FALSE);
                checkMenu(hWnd, ID_MPU6050_GYRO_2000, FALSE);
                b1 = MPU6050_I2C_SetScale(mpu6050Address, MPU6050_REG_GYRO_CONFIG, wmId - ID_MPU6050_GYRO_250);
                b2 = MPU6050_I2C_SetScale(mpu6050Address2, MPU6050_REG_GYRO_CONFIG, wmId - ID_MPU6050_GYRO_250);
                checkMenu(hWnd, wmId, b1 && b2);
                currentGyroscopeRatio = (wmId - ID_MPU6050_ACC_2G + 1) * 250;
                break;
            case ID_FICHIER_AUTO_INIT_68:
            case ID_FICHIER_AUTO_INIT_69:
            case ID_FICHIER_AUTO_INIT_BOTH:
                connectToUsb(hWnd, message, ID_FICHIER_CONNECT);
                if (wmId == ID_FICHIER_AUTO_INIT_68 || wmId == ID_FICHIER_AUTO_INIT_BOTH) {
                    mpu6050Address = 0x68;
                    connectMPU6050(hWnd, message, ID_FICHIER_MPU60500_0);
                    MPU6050_I2C_SetScale(mpu6050Address, MPU6050_REG_ACCEL_CONFIG, 0);
                    MPU6050_I2C_SetScale(mpu6050Address, MPU6050_REG_GYRO_CONFIG, 0);
                }
                if (wmId == ID_FICHIER_AUTO_INIT_69 || wmId == ID_FICHIER_AUTO_INIT_BOTH) {
                    mpu6050Address2 = 0x69;
                    connectMPU6050(hWnd, message, ID_FICHIER_MPU60500_0);
                    MPU6050_I2C_SetScale(mpu6050Address2, MPU6050_REG_ACCEL_CONFIG, 0);
                    MPU6050_I2C_SetScale(mpu6050Address2, MPU6050_REG_GYRO_CONFIG, 0);
                }
                currentAccelerationRatio = 2;
                currentGyroscopeRatio = 250;
                // No break here, just go on to read 
                wmId = ID_FICHIER_READ_1;
            case ID_FICHIER_READ_1:
            case ID_FICHIER_READ_2:
            case ID_FICHIER_READ_4:
            case ID_FICHIER_READ_8:
            case ID_FICHIER_READ_16:
            case ID_FICHIER_READ_32:
            case ID_FICHIER_READ_AVG_2:
            case ID_FICHIER_READ_AVG_4:
            case ID_FICHIER_READ_AVG_8:
            case ID_FICHIER_READ_AVG_16:
            case ID_FICHIER_READ_AVG_32:

                resetMenuRead(hWnd);
                SetCursor(LoadCursor(NULL, IDC_WAIT));
                realtimeMode = FALSE;
                realtimeIndex = 0;
                if (wmId >= ID_FICHIER_READ_AVG_2) {
                    b1 = loadSamplingData(wmId - ID_FICHIER_READ_AVG_1 + 1, MAX_SAMPLES, 0, TRUE);
                }
                else {
                    b1 = loadSamplingData(wmId - ID_FICHIER_READ_1 + 1, MAX_SAMPLES, 0, FALSE);
                }
                checkMenu(hWnd, wmId, b1);
                computeAllIntegrals();
                SetCursor(LoadCursor(NULL, IDC_ARROW));
                InvalidateRect(hWnd, NULL, TRUE);
                //MessageBox(hWnd, _T("Reading done"), _T("Information"), MB_OK);
                break;
            case ID_FICHIER_REALTIME_START_1:
            case ID_FICHIER_REALTIME_START_5:
            case ID_FICHIER_REALTIME_START_10:
            case ID_FICHIER_REALTIME_START_20:
            case ID_FICHIER_REALTIME_START_50:
            case ID_FICHIER_REALTIME_START_100:
                resetMenuRead(hWnd);
                checkMenu(hWnd, wmId, TRUE);
                switch (wmId) {
                case ID_FICHIER_REALTIME_START_1: realtimeAveraging = 1; break;
                case ID_FICHIER_REALTIME_START_5: realtimeAveraging = 5; break;
                case ID_FICHIER_REALTIME_START_10: realtimeAveraging = 10; break;
                case ID_FICHIER_REALTIME_START_20: realtimeAveraging = 20; break;
                case ID_FICHIER_REALTIME_START_50: realtimeAveraging = 50; break;
                case ID_FICHIER_REALTIME_START_100: realtimeAveraging = 100; break;
                }
                if (hThreadSampling) {
                    // realtimeAveraging has been updated and will be taken into account
                    // by the thread soon.
                    break;
                }
                else {
                    // let's go to ID_FICHIER_REALTIME_START_Thread 
                }
                // Do not set any break; here !!!
            case ID_FICHIER_REALTIME_START_Thread:
                if (hThreadSampling) {
                    break;
                }
                latestDisplaySampleIndex = 0;
                threadCurrentSampleIndex = 0;
                realtimeMode = TRUE;
                MPU6050_DisplayData = MPU6050_Samples;
                MPU6050_DisplayData2 = MPU6050_Samples2;
                hThreadSampling = CreateThread(
                    NULL,                   // default security attributes
                    0,                      // use default stack size  
                    SamplingThreadFunction,       // thread function name
                    nullptr,          // argument to thread function 
                    0,                      // use default creation flags 
                    &dwSamplingThreadId);   // returns the thread identifier 
                SetTimer(hWnd, ID_FICHIER_REALTIME_SAMPLE, 50, NULL);
                break;
            case ID_FICHIER_REALTIME_STOP:
                if (hThreadSampling) {
                    CloseHandle(hThreadSampling);
                    hThreadSampling = nullptr;
                }
                KillTimer(hWnd, ID_FICHIER_REALTIME_SAMPLE);
                realtimeMode = FALSE;
                computeAllIntegrals();
                break;
            case ID_FICHIER_CONNECT:
                connectToUsb(hWnd, message, wmId);
                break;
            case ID_SAVE_SAVE:
                save(hWnd);
                break;
            case ID_DRAW_ACC_1:
                showMPU6050_1_Acc = TRUE;
                showMPU6050_2_Acc = FALSE;
                showMPU6050_1_Gyro = FALSE;
                showMPU6050_2_Gyro = FALSE;
                CheckDlgButton(hScales, ID_CHANNEL_A1X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A1Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A1Z, BST_CHECKED);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_ACC_2:
                showMPU6050_1_Acc = FALSE;
                showMPU6050_2_Acc = TRUE;
                showMPU6050_1_Gyro = FALSE;
                showMPU6050_2_Gyro = FALSE;
                CheckDlgButton(hScales, ID_CHANNEL_A2X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A2Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A2Z, BST_CHECKED);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_ACC_12:
                showMPU6050_1_Acc = TRUE;
                showMPU6050_2_Acc = TRUE;
                showMPU6050_1_Gyro = FALSE;
                showMPU6050_2_Gyro = FALSE;
                CheckDlgButton(hScales, ID_CHANNEL_A1X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A1Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A1Z, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A2X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A2Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A2Z, BST_CHECKED);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_GYRO_1:
                showMPU6050_1_Acc = FALSE;
                showMPU6050_2_Acc = FALSE;
                showMPU6050_1_Gyro = TRUE;
                showMPU6050_2_Gyro = FALSE;
                CheckDlgButton(hScales, ID_CHANNEL_G1X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G1Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G1Z, BST_CHECKED);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_GYRO_2:
                showMPU6050_1_Acc = FALSE;
                showMPU6050_2_Acc = FALSE;
                showMPU6050_1_Gyro = FALSE;
                showMPU6050_2_Gyro = TRUE;
                CheckDlgButton(hScales, ID_CHANNEL_G2X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G2Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G2Z, BST_CHECKED);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_GYRO_12:
                showMPU6050_1_Acc = FALSE;
                showMPU6050_2_Acc = FALSE;
                showMPU6050_1_Gyro = TRUE;
                showMPU6050_2_Gyro = TRUE;
                CheckDlgButton(hScales, ID_CHANNEL_G1X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G1Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G1Z, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G2X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G2Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G2Z, BST_CHECKED);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_ACC_GYRO_1:
                showMPU6050_1_Acc = TRUE;
                showMPU6050_2_Acc = FALSE;
                showMPU6050_1_Gyro = TRUE;
                showMPU6050_2_Gyro = FALSE;
                CheckDlgButton(hScales, ID_CHANNEL_A1X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A1Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A1Z, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G1X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G1Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G1Z, BST_CHECKED);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_ACC_GYRO_2:
                showMPU6050_1_Acc = FALSE;
                showMPU6050_2_Acc = TRUE;
                showMPU6050_1_Gyro = FALSE;
                showMPU6050_2_Gyro = TRUE;
                CheckDlgButton(hScales, ID_CHANNEL_A2X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A2Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A2Z, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G2X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G2Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G2Z, BST_CHECKED);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_ALL:
                showMPU6050_1_Acc  = TRUE;
                showMPU6050_2_Acc  = TRUE;
                showMPU6050_1_Gyro = TRUE;
                showMPU6050_2_Gyro = TRUE;
                CheckDlgButton(hScales, ID_CHANNEL_A1X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A1Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A1Z, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G1X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G1Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G1Z, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A2X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A2Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_A2Z, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G2X, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G2Y, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_G2Z, BST_CHECKED);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_FILTER_RAW:
                filterByAverage = 0;
                checkMenu(hWnd, ID_FILTER_CENTER_0, TRUE);
                checkMenu(hWnd, ID_FILTER_AVG_3, FALSE);
                checkMenu(hWnd, ID_FILTER_AVG_5, FALSE);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_FILTER_AVG_3 :
                filterByAverage = 3;
                checkMenu(hWnd, ID_FILTER_CENTER_0, FALSE);
                checkMenu(hWnd, ID_FILTER_AVG_3, TRUE);
                checkMenu(hWnd, ID_FILTER_AVG_5, FALSE);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_FILTER_AVG_5:
                filterByAverage = 5;
                checkMenu(hWnd, ID_FILTER_CENTER_0, FALSE);
                checkMenu(hWnd, ID_FILTER_AVG_3, FALSE);
                checkMenu(hWnd, ID_FILTER_AVG_5, TRUE);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_FILTER_CENTER_0:
                centerDrawingToZero = TRUE;
                computeAverage(MPU6050_DisplayData,  &MPU6050_DisplayAverage , MAX_SAMPLES );
                computeAverage(MPU6050_DisplayData2, &MPU6050_DisplayAverage2, MAX_SAMPLES );
                computeAllIntegrals();
                checkMenu(hWnd, ID_FILTER_CENTER_0, TRUE);
                checkMenu(hWnd, ID_FILTER_NO_CENTER_0, FALSE);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_FILTER_NO_CENTER_0:
                centerDrawingToZero = FALSE;
                memset(&MPU6050_DisplayAverage , 0, sizeof(MPU6050_Measures));
                memset(&MPU6050_DisplayAverage2, 0, sizeof(MPU6050_Measures));
                computeAllIntegrals();
                checkMenu(hWnd, ID_FILTER_CENTER_0, FALSE);
                checkMenu(hWnd, ID_FILTER_NO_CENTER_0, TRUE);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_MEASURE:
                MPU6050_DisplayData  = MPU6050_Samples;
                MPU6050_DisplayData2 = MPU6050_Samples2;
                checkMenu(hWnd, ID_DRAW_MEASURE, TRUE);
                checkMenu(hWnd, ID_DRAW_INTEGRAL, FALSE);
                checkMenu(hWnd, ID_DRAW_INTEGRAL2, FALSE);
                checkMenu(hWnd, ID_DRAW_FFT_NOWINDOW, FALSE);
                xLegendScale = xLegendScale2 = 0;
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_INTEGRAL:
                MPU6050_DisplayData  = MPU6050_Speed;
                MPU6050_DisplayData2 = MPU6050_Speed2;
                checkMenu(hWnd, ID_DRAW_MEASURE, FALSE);
                checkMenu(hWnd, ID_DRAW_INTEGRAL, TRUE);
                checkMenu(hWnd, ID_DRAW_INTEGRAL2, FALSE);
                checkMenu(hWnd, ID_DRAW_FFT_NOWINDOW, FALSE);
                xLegendScale = xLegendScale2 = 0;
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_INTEGRAL2:
                MPU6050_DisplayData  = MPU6050_Position;
                MPU6050_DisplayData2 = MPU6050_Position2;
                checkMenu(hWnd, ID_DRAW_MEASURE, FALSE);
                checkMenu(hWnd, ID_DRAW_INTEGRAL, FALSE);
                checkMenu(hWnd, ID_DRAW_INTEGRAL2, TRUE);
                checkMenu(hWnd, ID_DRAW_FFT_NOWINDOW, FALSE);
                xLegendScale = xLegendScale2 = 0;
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_SAVE_RELOAD:
                if (reload(hWnd)) {
                }
                mpu6050Address = 68;
                mpu6050Address = 69;
                xCenter = 0;
                xZoom = 1;
                zoom = ZOOM_UNITY;
                computeAllIntegrals();
                MPU6050_DisplayData = MPU6050_Samples;
                MPU6050_DisplayData2 = MPU6050_Samples2;
                checkMenu(hWnd, ID_DRAW_MEASURE, TRUE);
                checkMenu(hWnd, ID_DRAW_INTEGRAL, FALSE);
                checkMenu(hWnd, ID_DRAW_INTEGRAL2, FALSE);
                checkMenu(hWnd, ID_DRAW_FFT_NOWINDOW, FALSE);
                InvalidateRect(hWnd, NULL, TRUE);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_SAVE_TEST_INTEGRAL:
                testIntegral();
                computeAllIntegrals();
                mpu6050Address  = 0x68;
                mpu6050Address2 = 0x69;
                CheckDlgButton(hScales, ID_CHANNEL_DETAILS_1, BST_CHECKED);
                CheckDlgButton(hScales, ID_CHANNEL_DETAILS_2, BST_CHECKED);
                InvalidateRect(hWnd, NULL, TRUE);
                break;

            case ID_CHANNEL_A1X :
            case ID_CHANNEL_A1Y :
            case ID_CHANNEL_A1Z :
            case ID_CHANNEL_G1X :
            case ID_CHANNEL_G1Y :
            case ID_CHANNEL_G1Z :
            case ID_CHANNEL_A2X :
            case ID_CHANNEL_A2Y :
            case ID_CHANNEL_A2Z :
            case ID_CHANNEL_G2X :
            case ID_CHANNEL_G2Y :
            case ID_CHANNEL_G2Z :
            case ID_CHANNEL_DETAILS_1:
            case ID_CHANNEL_DETAILS_2:
                InvalidateRect(mainHWnd, NULL, TRUE);
                break;
            case ID_DRAW_FFT_NOWINDOW:
            case ID_DRAW_FFT_TRIANGLE:
            case ID_DRAW_FFT_HANN:
            case ID_DRAW_FFT_HAMMING:
            case ID_DRAW_FFT_BLACKMAN:
                checkMenu(hWnd, ID_DRAW_MEASURE, FALSE);
                checkMenu(hWnd, ID_DRAW_INTEGRAL, FALSE);
                checkMenu(hWnd, ID_DRAW_INTEGRAL2, FALSE);
                //checkMenu(hWnd, wmId, TRUE); 
                memset(MPU6050_FFT, 0, sizeof(MPU6050_FFT));
                memset(MPU6050_FFT2, 0, sizeof(MPU6050_FFT2));
                for (int channel = 0; channel < 6; channel++) {
                    int windowType = wmId - ID_DRAW_FFT_NOWINDOW;
                    doFFT(MPU6050_Samples, MAX_SAMPLES, MPU6050_FFT, channel, windowType);
                    doFFT(MPU6050_Samples2, MAX_SAMPLES, MPU6050_FFT2, channel, windowType);
                }
                MPU6050_DisplayData  = MPU6050_FFT;
                MPU6050_DisplayData2 = MPU6050_FFT2;
                xLegendScale = getFFTScaleMilliHertz(MPU6050_Samples, MAX_SAMPLES);
                xLegendScale2 = getFFTScaleMilliHertz(MPU6050_Samples2, MAX_SAMPLES);
                xZoom = 1;
                zoom = ZOOM_UNITY;
                xCenter = 0;
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_DUPLICATE_12:
                memcpy_s(MPU6050_Samples2, sizeof(MPU6050_Samples2), MPU6050_Samples, sizeof(MPU6050_Samples));
                mpu6050Address2 = 0x69;
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_DRAW_DUPLICATE_21:
                memcpy_s(MPU6050_Samples, sizeof(MPU6050_Samples), MPU6050_Samples2, sizeof(MPU6050_Samples2));
                mpu6050Address = 0x68;
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_SAVE_PYTHON3:
                if(saveSamples( (char*)"raw")){
                    // Recommended python 3.9 or later
                    // pip install matplotlib
                    UINT r = WinExec("python MPU6050.py raw_68.csv raw_69.csv", SW_SHOWMAXIMIZED);
                    switch (r) {
                        case 0 : MessageBox(hWnd, _T("The system is out of memory or resources."), _T("Error"), MB_OK); break;
                        case ERROR_BAD_FORMAT: MessageBox(hWnd, _T("The.exe file is invalid."), _T("Error"), MB_OK);  break;
                        case ERROR_FILE_NOT_FOUND : MessageBox(hWnd, _T("The specified file was not found."), _T("Error"), MB_OK);  break;
                        case ERROR_PATH_NOT_FOUND : MessageBox(hWnd, _T("The specified path was not found. "), _T("Error"), MB_OK); break;
                    }
                }
                break;
            case ID_ZOOM_YZOOM:
                userNumberInput = 0;
                usernumberInputTarget = 0;
                break;
            case ID_ZOOM_SPLIT :
                splitY = 200;
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_ZOOM_SPLIT_LARGE:
splitY = 400;
InvalidateRect(hWnd, NULL, TRUE);
break;
            case ID_ZOOM_UNSPLIT:
                splitY = 0;
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_ZOOM_Y2_BY_1:
            case ID_ZOOM_Y2_BY_2:
            case ID_ZOOM_Y2_BY_5:
            case ID_ZOOM_Y2_BY_10:
            case ID_ZOOM_Y2_BY_20:
                zoom2 = wmId - ID_ZOOM_Y2_BY_1 + 1;
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_ZOOM_X_EQ_0:
                index100usAtCenter = 0;
                xCenter = 0;
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_ZOOM_X_BY_1://     33301
            case ID_ZOOM_X_BY_2://     33302
            case ID_ZOOM_X_BY_4://    33304
            case ID_ZOOM_X_BY_8://    33308
            case ID_ZOOM_X_BY_16://   33316
            case ID_ZOOM_X_BY_32://  33332
            case ID_ZOOM_X_BY_64://   33364
                xZoom = wmId - ID_ZOOM_X_BY_1 + 1;
                InvalidateRect(hWnd, NULL, TRUE);
                break;

            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_TIMER:
        if (hThreadSampling) {
            realtimeDrawScale = TRUE;
            RECT rect;
            rect.top = 0;
            rect.bottom = 1024;
            // Xzoom does not work because index100usAtCenter or xCenter should be used to calculate rectangle to clean)
            rect.left = latestDisplaySampleIndex * xZoom - xCenter * (xZoom - 1);
            rect.right = threadCurrentSampleIndex * xZoom + 20 - xCenter * (xZoom - 1); // Add littl white
            latestDisplaySampleIndex = threadCurrentSampleIndex;
            InvalidateRect(hWnd, &rect, TRUE);
        }
        break;

    case WM_PAINT:
    {
        if (hWnd == hScales) {
            if (!realtimeMode)
            {
                PAINTSTRUCT ps2;
                HDC hdcScales = BeginPaint(hScales, &ps2);
                drawDetails(hScales, hdcScales, 0, 10);
                EndPaint(hScales, &ps2);
            }
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
        PAINTSTRUCT ps;
        RECT rect;
        HDC hdc = BeginPaint(hWnd, &ps);

        GetWindowRect(hWnd, &rect);
        int y0 = (rect.bottom - rect.top) / 2;

        if (splitY) {
            y0 = y0 - splitY / 2;
        }
        int y1 = y0 + splitY;

        xAxle1YPos = y0;
        xAxle2YPos = y1;

        if (realtimeDrawScale || !realtimeMode) {
            FillRect(hdc, &rect, (HBRUSH)GetStockObject(WHITE_BRUSH));
        }

        if (mpu6050Address != 0) {
            if (showMPU6050_1_Acc) {
                bool bx = IsDlgButtonChecked(hScales, ID_CHANNEL_A1X) == BST_CHECKED;
                bool by = IsDlgButtonChecked(hScales, ID_CHANNEL_A1Y) == BST_CHECKED;
                bool bz = IsDlgButtonChecked(hScales, ID_CHANNEL_A1Z) == BST_CHECKED;
                if (IsDlgButtonChecked(hScales, ID_CHANNEL_DETAILS_1) == BST_CHECKED) {
                    drawSamplingAccData(hWnd, hdc, 0, y0, MPU6050_DisplayData, bx, by, bz);
                }
            }
            if (showMPU6050_1_Gyro) {
                bool bx = IsDlgButtonChecked(hScales, ID_CHANNEL_G1X) == BST_CHECKED;
                bool by = IsDlgButtonChecked(hScales, ID_CHANNEL_G1Y) == BST_CHECKED;
                bool bz = IsDlgButtonChecked(hScales, ID_CHANNEL_G1Z) == BST_CHECKED;
                if (IsDlgButtonChecked(hScales, ID_CHANNEL_DETAILS_1) == BST_CHECKED) {
                    drawSamplingGyroData(hWnd, hdc, 0, y0, MPU6050_DisplayData, bx, by, bz);
                }
            }
        }
        if (mpu6050Address2 != 0) {
            if (showMPU6050_2_Acc) {
                bool bx = IsDlgButtonChecked(hScales, ID_CHANNEL_A2X) == BST_CHECKED;
                bool by = IsDlgButtonChecked(hScales, ID_CHANNEL_A2Y) == BST_CHECKED;
                bool bz = IsDlgButtonChecked(hScales, ID_CHANNEL_A2Z) == BST_CHECKED;
                if (IsDlgButtonChecked(hScales, ID_CHANNEL_DETAILS_2) == BST_CHECKED) {
                    drawSamplingAccData(hWnd, hdc, 0, y1, MPU6050_DisplayData2, bx, by, bz);
                }
            }
            if (showMPU6050_2_Gyro) {
                bool bx = IsDlgButtonChecked(hScales, ID_CHANNEL_G2X) == BST_CHECKED;
                bool by = IsDlgButtonChecked(hScales, ID_CHANNEL_G2Y) == BST_CHECKED;
                bool bz = IsDlgButtonChecked(hScales, ID_CHANNEL_G2Z) == BST_CHECKED;
                if (IsDlgButtonChecked(hScales, ID_CHANNEL_DETAILS_2) == BST_CHECKED) {
                    drawSamplingGyroData(hWnd, hdc, 0, y1, MPU6050_DisplayData2, bx, by, bz);
                }
            }
        }
            if (realtimeDrawScale ||  !realtimeMode) {
                drawScales(hWnd, hdc, 0, y0);
                if (y1 != y0) {
                    drawScales(hWnd, hdc, 0, y1);
                }
            }
            if (!realtimeMode) {
                drawMousePosition(hWnd, hdc, 0, (rect.bottom - rect.top) / 2);
                if (usernumberInputTarget >= 0) {
                    drawInput(hdc);
                }
            }
            EndPaint(hWnd, &ps);

        }
        break;
    case WM_DESTROY:
        if (mIndex >= 0) {
            USBIO_CloseDevice(mIndex);
        }
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Gestionnaire de messages pour la boîte de dialogue À propos de.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
