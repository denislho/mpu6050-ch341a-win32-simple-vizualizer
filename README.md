# Win32 simple vizualizer

(New import because cannot set private project to public)

Prerequisites :
- Recent Windows 
- CH431A USB to SPI and I2C module, used in I2C in this software
- 1 or 2 MPU6050 connected by I2C to CH431A. CH431A connected by USB to PC. Please to not forget to check pull-up resistors !
- A big screen ;-)


Very simple Win32 program that was designed to be easily modified by students.
- It simply grabs data from one or two I2C MPU6050 using an I2C to USB device CH341A.
- It uses some simplifactions to make source code as simple as possible :

- data rate is stable (so no display when sampling serious data)
- buffer is 2048 samples long
- Graphics are simple

It performs very simple tasks :

- Set data rate
- Set accelerometer range
- Set gyroscope range
- X zoom is 1,2,4,8,16,32,64
- grab data at maximum speed the PC can do
- grab data at slower rate to increase sampling time

Display :

- Accelerometer and or gyroscope
- Possibility to choose X,Y,Z for both accelerometer and gyroscope
- Filtering using average 3 or 5 points (to see how it performs if you would like to embeded it later)
- One graphic, splitable into 2 graphics
- read and export data in CSV
- Can call python for additional processing

Please use a mouse with a wheel !!!

- left click to place a first vertical bar (data at the given X position is displayed)
- right click to place a second vertical bar (data at the given X position is displayed)
-  left click to place an horizontal bar
-  right click to place an horizontal bar
- mouse wheel to Y zoom

Please use menu for features

- split data using 2 axles
- X Zoom
- Y2 Zoom (convenient to zoom aceelerometer and gyroscope differently)

